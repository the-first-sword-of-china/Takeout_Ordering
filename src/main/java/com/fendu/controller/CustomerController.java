package com.fendu.controller;

import com.fendu.entity.Customer;
import com.fendu.entity.Table;
import com.fendu.service.intf.CustomerService;
import com.fendu.service.intf.TableService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customer/")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    //新增
    @PostMapping("save")
    public R save(Customer customer){
        return customerService.insertCustomerService(customer);
    }
    //查询
    @GetMapping("all")
    public PageVo<Customer> selectAllList(int page, int limit){
        return customerService.selectCustomerService(page, limit);
    }
    //修改
    @PostMapping("update")
    public R update(Customer customer){
        return customerService.updateCustomerService(customer);
    }
    //删除
    @PostMapping("delete")
    public R delete(int cust_id){
        return customerService.deleteCustomerService(cust_id);
    }
}
