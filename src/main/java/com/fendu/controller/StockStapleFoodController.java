package com.fendu.controller;


import com.fendu.entity.StockStapleFood;
import com.fendu.service.intf.StockStapleService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 锤子
 * @data 2021/10/6 0:00
 * @projectname LoginDemo
 */
@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/stapleFood/")
public class StockStapleFoodController {
    @Autowired
    private StockStapleService  stockStapleService;
    //新增
    @PostMapping("save")
    public R save(StockStapleFood stockStapleFood){
        return stockStapleService.insertStockStapleService(stockStapleFood);
    }

    //查询
    @GetMapping("all")
    public PageVo<StockStapleFood> selectAllList(int page, int limit){

            return stockStapleService.selectStockStapleService(page, limit);
    }

    //修改
    @PostMapping("update")
    public R update(StockStapleFood  stockStapleFood){
        return stockStapleService.updateStockStapleService(stockStapleFood);
    }

    //删除
    @PostMapping("delete")
    public R delete(int staple_id){
        return stockStapleService.deleteStockStapleService(staple_id);
    }


}
