package com.fendu.controller;

import com.fendu.entity.HavePaid;
import com.fendu.service.intf.HavePaidService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/HavePaid/")
public class HavePaidController {
    @Autowired
    private HavePaidService havePaidService;
    //增
    @PostMapping("add")
    public R add(HavePaid havePaid){return havePaidService.addService(havePaid);}
    //删
    @PostMapping("del")
    public R del(int id){return havePaidService.delService(id);}
    //改
    @PostMapping("upd")
    public R upd(HavePaid havePaid){return havePaidService.updService(havePaid);}
    //查
    @GetMapping("sel")
    public PageVo<HavePaid> sel(int page, int limit){return havePaidService.selService(page,limit);}
}
