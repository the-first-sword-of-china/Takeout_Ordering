package com.fendu.controller;

import com.fendu.entity.FinaWeek;
import com.fendu.entity.StoresInformation;
import com.fendu.service.intf.FinaWeekService;
import com.fendu.service.intf.StoresInformationService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/storesInformation/")
public class StoresInformationController {
    @Autowired
    private StoresInformationService storesInformationService;
    @GetMapping("all")
    public PageVo<StoresInformation> selectAllList(int page, int limit){
        return storesInformationService.selectStoresInformationService(page,limit);
    }

    //新增
    @PostMapping("save")
    public R save(StoresInformation storesInformation){
        return storesInformationService.insertStoresInformationService(storesInformation);
    }

    //修改
    @PostMapping("update")
    public R update(StoresInformation storesInformation){
        return storesInformationService.updateStoresInformationService(storesInformation);

    }

    @PostMapping("delete")
    public R delete(int stores_id){
        return storesInformationService.deleteStoresInformationService(stores_id);
    }
}
