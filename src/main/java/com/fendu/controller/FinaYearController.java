package com.fendu.controller;

import com.fendu.entity.FinaMonth;
import com.fendu.entity.FinaYear;
import com.fendu.service.intf.FinaYearService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/finaYear/")
public class FinaYearController {
    @Autowired
    private FinaYearService finaYearService;
    @GetMapping("all")
    public PageVo<FinaYear> selectAllList(int page, int limit){
        return finaYearService.selectFinaYearService(page,limit);
    }

    //新增
    @PostMapping("save")
    public R save(FinaYear finaYear){
        return finaYearService.insertFinaYearService(finaYear);
    }

    //修改
    @PostMapping("update")
    public R update(FinaYear finaYear){
        return finaYearService.updateFinaYearService(finaYear);

    }

    @PostMapping("delete")
    public R delete(int fina_year_id){

        return finaYearService.deleteFinaYearService(fina_year_id);
    }
}
