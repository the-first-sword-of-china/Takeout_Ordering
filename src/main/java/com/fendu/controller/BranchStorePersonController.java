package com.fendu.controller;

import com.fendu.entity.BranchFinanYear;
import com.fendu.entity.BranchStorePerson;
import com.fendu.service.intf.BranchFinanYearService;
import com.fendu.service.intf.BranchStorePersonService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/branchStorePerson/")
public class BranchStorePersonController {
    @Autowired
    private BranchStorePersonService branchStorePersonService;
    @PostMapping("save")
    public R save(BranchStorePerson branchStorePerson){
        return branchStorePersonService.insertService(branchStorePerson);
    }

    @GetMapping("all")
    public PageVo<BranchStorePerson> selectAlllist(int page,int limit){
        System.out.println(page);
        return branchStorePersonService.selectService(page,limit);
    }
    @PostMapping("update")
    public R update(BranchStorePerson branchStorePerson){
        return branchStorePersonService.updateService(branchStorePerson);
    }
    @PostMapping("delete")
    public R delete(int storePerson_id){
        return branchStorePersonService.deleteService(storePerson_id);
    }
}
