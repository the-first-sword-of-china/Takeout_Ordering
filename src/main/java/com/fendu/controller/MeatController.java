package com.fendu.controller;


import com.fendu.entity.HotDishes;
import com.fendu.entity.Meat;
import com.fendu.service.intf.MeatService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 锤子
 * @data 2021/10/7 18:01
 * @projectname Takeout_Ordering
 */

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/meat/")
public class MeatController {
    @Autowired MeatService meatService;
    //查询
    @GetMapping("all")
    public PageVo<Meat> selectAllList(int page, int limit){

        return meatService.selectMeatService(page, limit);
    }
    //新增
    @PostMapping("save")
    public R save(Meat meat){
        return meatService.insertMeatService(meat);
    }
    //删除
    @PostMapping("delete")
    public R delectAll(int meat_id){

        return meatService.delectMeatService(meat_id);
    }
    //修改
    @PostMapping("update")
    public R update(Meat meat){
        return meatService.updateMeatService(meat);
    }

}
