package com.fendu.controller;


import com.fendu.entity.Meat;
import com.fendu.entity.RatePerson;
import com.fendu.service.intf.MeatService;
import com.fendu.service.intf.RatePersonService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 锤子
 * @data 2021/10/7 18:01
 * @projectname Takeout_Ordering
 */

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/person/")
public class RatePersonController {
    @Autowired
   private RatePersonService ratePersonService;
    //查询
    @GetMapping("all")
    public PageVo<RatePerson> selectAllList(int page, int limit){

        return ratePersonService.selectPersonService(page, limit);
    }
    //新增
    @PostMapping("save")
    public R save(RatePerson  ratePerson){
        return ratePersonService.insertPersonService(ratePerson);
    }
    //删除
    @PostMapping("delete")
    public R delectAll(int rate_id){

        return ratePersonService.delectPersonService(rate_id);
    }
    //修改
    @PostMapping("update")
    public R update(RatePerson  ratePerson){
        return ratePersonService.updatePersonService(ratePerson);
    }

}
