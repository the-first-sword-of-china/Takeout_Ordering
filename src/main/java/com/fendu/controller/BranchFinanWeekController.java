package com.fendu.controller;

import com.fendu.entity.BranchFinanWeek;
import com.fendu.service.intf.BranchFinanWeekService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/BranchFinanWeek/")
public class BranchFinanWeekController {
    @Autowired
    private BranchFinanWeekService branchFinanWeekService;
    //增
    @PostMapping("add")
    public R add(BranchFinanWeek branchFinanWeek){return branchFinanWeekService.addService(branchFinanWeek);}
    //删
    @PostMapping("del")
    public R del(int id){return branchFinanWeekService.delService(id);}
    //改
    @PostMapping("upd")
    public R upd(BranchFinanWeek branchFinanWeek){return branchFinanWeekService.updService(branchFinanWeek);}
    //查
    @GetMapping("sel")
    public PageVo<BranchFinanWeek>sel(int page,int limit){return branchFinanWeekService.selService(page,limit);}
}
