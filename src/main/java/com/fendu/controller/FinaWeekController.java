package com.fendu.controller;

import com.fendu.entity.FinaWeek;
import com.fendu.service.intf.FinaWeekService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/finaWeek/")
public class FinaWeekController {
    @Autowired
    private FinaWeekService finaWeekService;
    @GetMapping("all")
    public PageVo<FinaWeek> selectAllList(int page, int limit){
        return finaWeekService.selectFinaWeekService(page,limit);
    }

    //新增
    @PostMapping("save")
    public R save(FinaWeek finaWeek){
        return finaWeekService.insertFinaWeekService(finaWeek);
    }

    //修改
    @PostMapping("update")
    public R update(FinaWeek finaWeek){
        return finaWeekService.updateFinaWeekService(finaWeek);

    }

    @PostMapping("delete")
    public R delete(int fina_week_id){
        return finaWeekService.deleteFinaWeekService(fina_week_id);
    }
}
