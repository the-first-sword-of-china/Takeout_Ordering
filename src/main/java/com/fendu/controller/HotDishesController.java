package com.fendu.controller;


import com.fendu.entity.HotDishes;
import com.fendu.service.intf.HotDishesService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author 锤子
 * @data 2021/10/6 0:00
 * @projectname LoginDemo
 */
@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/hotDishes/")
public class HotDishesController {
    @Autowired
    private HotDishesService hotDishesService;
    //新增
    @PostMapping("save")
    public R save(HotDishes hotDishes){
        return hotDishesService.insertHotService(hotDishes);
    }

    //查询
    @GetMapping("all")
    public PageVo<HotDishes> selectAllList(int page, int limit){

            return hotDishesService.selectHotService(page, limit);
    }

    //修改
    @PostMapping("update")
    public R update(HotDishes hotDishes){
        return hotDishesService.updateHotService(hotDishes);
    }

    //删除
    @PostMapping("delete")
    public R delete(int hot_id){
        return hotDishesService.deleteHotService(hot_id);
    }


}
