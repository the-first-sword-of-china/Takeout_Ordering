package com.fendu.controller;

import com.fendu.entity.Admin;
import com.fendu.service.intf.AdminService;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/api/admin/")
public class AdminController {

    @Autowired
    private AdminService service;
    //登录
    @PostMapping("login")
    public R login(String adminName, String adminPassword, HttpSession session){
        return service.login(adminName,adminPassword,session);
    }
    //修改
    @PostMapping("updateAdmin")
    public R update(Admin admin){
        return service.update(admin);
    }
    //改密码
    @PostMapping("changePassword")
    public R change(String oldPassword,String newPassword,HttpSession session){
        return service.changePassword(oldPassword,newPassword,session);
    }

}
