package com.fendu.controller;
import com.fendu.entity.BranchFinanMonth;
import com.fendu.service.intf.BranchFinanMonthService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/BranchFinanMonth/")
public class BranchFinanMonthController {
    @Autowired
    private BranchFinanMonthService branchFinanMonthService;
    //增
    @PostMapping("add")
    public R add(BranchFinanMonth branchFinanMonth){return branchFinanMonthService.addService(branchFinanMonth);}
    //删
    @PostMapping("del")
    public R del(int id){return branchFinanMonthService.delService(id);}
    //改
    @PostMapping("upd")
    public R upd(BranchFinanMonth branchFinanMonth){return branchFinanMonthService.updService(branchFinanMonth);}
    //查
    @GetMapping("sel")
    public PageVo<BranchFinanMonth> sel(int page, int limit){return branchFinanMonthService.selService(page,limit);}

}
