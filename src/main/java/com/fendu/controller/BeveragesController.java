package com.fendu.controller;


import com.fendu.entity.Beverages;
import com.fendu.entity.HotDishes;
import com.fendu.service.intf.BeveragesService;
import com.fendu.service.intf.HotDishesService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 锤子
 * @data 2021/10/6 0:00
 * @projectname LoginDemo
 */
@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/beverages/")
public class BeveragesController {
    @Autowired
    private BeveragesService beveragesService;
    //新增
    @PostMapping("save")
    public R save(Beverages beverages){

        return beveragesService.insterBeverService(beverages);
    }



    //查询
    @GetMapping("all")
    public PageVo<Beverages> selectAllList(int page, int limit){

            return beveragesService.selectBeverService(page, limit);
    }

}
