package com.fendu.controller;

import com.fendu.entity.StockSeafood;
import com.fendu.service.intf.StockSeafoodService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/StockSeafood/")
public class StockSeafoodController {
    @Autowired
    private StockSeafoodService stockSeafoodService;

    //新增
    @PostMapping("saveSeafood")
    public R saveSeafood(StockSeafood stockSeafood){
        return stockSeafoodService.insterSeafood(stockSeafood);
    }

    //查询
    @GetMapping("selectSeafoodAllList")
    public PageVo<StockSeafood> selectSeafoodAllList(int page, int limit){
        return stockSeafoodService.selectStockSeafoodAll(page,limit);
    };

    //删除
    @PostMapping("deleteSeafoodById")
    public R deleteSeafoodById(int id){
        return stockSeafoodService.deleteSeafoodById(id);
    }

    //修改
    @PostMapping("updateSeafood")
    public R updateSeafood(StockSeafood stockSeafood){
        return stockSeafoodService.updateSeafood(stockSeafood);
    }
}
