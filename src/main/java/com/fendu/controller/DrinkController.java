package com.fendu.controller;



import com.fendu.entity.Drink;
import com.fendu.service.intf.DrinkService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/drink/")
public class DrinkController {
    @Autowired
    private DrinkService service;
    @PostMapping("save")
    public R save(Drink drink){
        return service.save(drink);
    }
    @GetMapping("page")
    public PageVo<Drink> page(int page,int limit){
        return service.queryPage(page,limit);
    }

    @PostMapping("delete")
    public R delete(int beve_numid){
        return service.delete(beve_numid);
    }

    @PostMapping("update")
    public R update(Drink drink){
        System.out.println(drink);
        return service.update(drink);
    }
}
