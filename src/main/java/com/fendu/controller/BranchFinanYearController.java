package com.fendu.controller;

import com.fendu.entity.BranchFinanYear;
import com.fendu.service.intf.BranchFinanYearService;
import com.fendu.service.intf.BranchStorePersonService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/branchFinanYear/")
public class BranchFinanYearController {
    @Autowired
    private BranchFinanYearService branchFinanYearService;
    @PostMapping("save")
    public R save(BranchFinanYear branchFinanYear){
        return branchFinanYearService.insertBranchYearService(branchFinanYear);
    }

    @GetMapping("all")
    public PageVo<BranchFinanYear> selectAlllist(int page,int limit){
        return branchFinanYearService.selectBranchYearService(page,limit);
    }
    @PostMapping("update")
    public R update(BranchFinanYear branchFinanYear){
        return branchFinanYearService.updateBranchYearService(branchFinanYear);
    }
    @PostMapping("delete")
    public R delete(int finanyear_id){
        return branchFinanYearService.deleteBranchYearService(finanyear_id);
    }
}
