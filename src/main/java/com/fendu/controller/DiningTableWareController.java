package com.fendu.controller;

import com.fendu.entity.DiningTableWare;
import com.fendu.service.intf.DiningTableWareService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/diningTableWare/")
public class DiningTableWareController {
    @Autowired
    private DiningTableWareService diningTableWareService;

    //新增
    @PostMapping("saveWare")
    public R saveWare(DiningTableWare diningTableWare){
        return diningTableWareService.insterDiningWare(diningTableWare);
    }

    //查询
    @GetMapping("selectWareAllList")
    public PageVo<DiningTableWare> selectWareAllList(int page, int limit){
        return diningTableWareService.selectDiningWareService(page,limit);
    };

    //删除
    @PostMapping("deleteWareById")
    public R deleteWareById(int id){
        return diningTableWareService.deleteTableWareById(id);
    }

    //修改
    @PostMapping("updateWare")
    public R updateWare(DiningTableWare diningTableWare){
        return diningTableWareService.updateDiningWare(diningTableWare);
    }

}
