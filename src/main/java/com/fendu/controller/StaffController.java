package com.fendu.controller;

import com.fendu.entity.HotDishes;
import com.fendu.entity.Staff;
import com.fendu.service.intf.StaffService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/staff/")
public class StaffController {
    @Autowired
    private StaffService staffService;
    @GetMapping("all")
    public PageVo<Staff> selectAllList(int page, int limit){
        return staffService.selectStaffService(page,limit);
    }

    //新增
    @PostMapping("save")
    public R save(Staff staff){
        return staffService.insertStaffService(staff);
    }

    //修改
    @PostMapping("update")
    public R update(Staff staff){
        return staffService.updateStaffService(staff);
    }

    @PostMapping("delete")
    public R delete(int staff_id){
        return staffService.deleteStaffService(staff_id);
    }

}
