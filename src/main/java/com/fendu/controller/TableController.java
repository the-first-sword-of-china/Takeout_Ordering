package com.fendu.controller;

import com.fendu.bo.TableBo;
import com.fendu.entity.Table;
import com.fendu.service.intf.TableService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/table/")
public class TableController {
    @Autowired
    private TableService tableService;

    //新增
    @PostMapping("save")
    public R save(Table table) {
        return tableService.insertTableService(table);
    }

    //查询1.0
//    @GetMapping("all")
//    public PageVo<Table> selectAllList(int page, int limit) {
//        return tableService.selectTableService(page, limit);
//    }

    //查询2.0
    @GetMapping("all")
    public PageVo<Table> selectAllList2(TableBo tableBo) {
        return tableService.selectTableService2(tableBo);
    }

    //修改
    @PostMapping("update")
    public R update(Table table) {
        return tableService.updateTableService(table);
    }

    //删除
    @PostMapping("delete")
    public R delete(int table_id) {
        return tableService.deleteTableService(table_id);
    }
    //文件上传
    @PostMapping("batchadd")
    public R batch(MultipartFile file){
        return tableService.batchAdd(file);
    }
    //导出excel文件
    @GetMapping("download")
    public void download(TableBo tableBo, HttpServletResponse response){
        tableService.exportExcel(tableBo,response);
    }
}
