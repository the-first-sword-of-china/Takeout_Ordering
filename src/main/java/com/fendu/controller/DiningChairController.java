package com.fendu.controller;

import com.fendu.bo.DiningChairBo;
import com.fendu.entity.DiningChair;
import com.fendu.service.intf.DiningChairService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/diningChair/")
public class DiningChairController {
    @Autowired
    private DiningChairService diningChairService;

    //新增
    @PostMapping("saveChair")
    public R saveChair(DiningChair diningChair){
        return diningChairService.insterDiningChair(diningChair);
    }

    //查询
//    @GetMapping("selectChairAllList")
//    public PageVo<DiningChair> selectChairAllList(int page, int limit){
//        return diningChairService.selectDiningChairService(page,limit);
//    };
    //条件查询
    @GetMapping("selectChairAllList")
    public PageVo<DiningChair> selectChairAllList(DiningChairBo bo){
        return diningChairService.selectDiningChairByCd(bo);
    };

    //删除
    @PostMapping("deleteChairById")
    public R deleteChairById(int id){
        return diningChairService.deleteDiningChairById(id);
    }

    //修改
    @PostMapping("updateChair")
    public R updateChair(DiningChair diningChair){
        return diningChairService.updateDiningChair(diningChair);
    }

}
