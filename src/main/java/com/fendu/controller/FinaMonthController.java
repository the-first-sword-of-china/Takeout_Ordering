package com.fendu.controller;

import com.fendu.bo.FinaMonthBo;
import com.fendu.entity.FinaMonth;
import com.fendu.entity.FinaWeek;
import com.fendu.service.intf.FinaMonthService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/api/finaMonth/")
public class FinaMonthController {
    @Autowired
    private FinaMonthService finaMonthService;
    @GetMapping("all")
    public PageVo<FinaMonth> selectAllList(int page, int limit){
        return finaMonthService.selectFinaMonthService(page,limit);
    }

    //新增
    @PostMapping("save")
    public R save(FinaMonth finaMonth){
        return finaMonthService.insertFinaMonthService(finaMonth);
    }
    @GetMapping("page")
    public PageVo<FinaMonth> page(FinaMonthBo finaMonthBo){
        return finaMonthService.selectFinaMonthWhereService(finaMonthBo);

    }

    //修改
    @PostMapping("update")
    public R update(FinaMonth finaMonth){
        return finaMonthService.updateFinaMonthService(finaMonth);

    }

    @PostMapping("delete")
    public R delete(int fina_mon_id){

        return finaMonthService.deleteFinaMonthService(fina_mon_id);
    }
    @PostMapping("batchadd")
    public R batch(MultipartFile multipartFile){
        return finaMonthService.batchAdd(multipartFile);
    }
    @GetMapping("download")
    public void download(FinaMonthBo finaMonthBo, HttpServletResponse httpServletResponse){
        finaMonthService.exportExcel(finaMonthBo,httpServletResponse);

    }
}
