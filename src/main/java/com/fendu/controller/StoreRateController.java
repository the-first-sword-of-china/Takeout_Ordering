package com.fendu.controller;

import com.fendu.entity.StoreRate;
import com.fendu.service.intf.StoreRateService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/StoreReate")
public class StoreRateController {
    @Autowired
    private StoreRateService storeRateService;

    //增
    @PostMapping("/add")
    public R add(StoreRate storeRate) {
        return storeRateService.addService(storeRate);
    }

    //删
    @PostMapping("/del")
    public R del(int id) {
        return storeRateService.delService(id);
    }

    //改
    @PostMapping("/upd")
    public R upd(StoreRate storeRate) {
        return storeRateService.updService(storeRate);
    }

    //查
    @GetMapping("/sel")
    public PageVo<StoreRate> sel(int page, int limit) {
        return storeRateService.selService(page, limit);
    }
}
