package com.fendu.controller;

import com.fendu.entity.ColdDishes;
import com.fendu.service.intf.ColdDishesService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/coldDishes/")
public class ColdDishesController {

    @Autowired
    private ColdDishesService coldDishesService;

    //新增
    @PostMapping("save")
    public R save(ColdDishes coldDishes){

        return coldDishesService.insterClodService(coldDishes);
    }

    //查询
    @GetMapping("all")
    public PageVo<ColdDishes> selectAllList(int page, int limit){

        return coldDishesService.selectColdService(page,limit);

    };

    //删除
    @PostMapping("deleteColdDishesById")
    public R deleteColdById(int id){

        return coldDishesService.deleteColdById(id);

    }

    //修改
    @PostMapping("updateColdById")
    public R updateCold(ColdDishes coldDishes){
        return coldDishesService.updateCold(coldDishes);
    }
}
