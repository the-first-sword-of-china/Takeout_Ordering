package com.fendu.controller;

import com.fendu.entity.ColdDishes;
import com.fendu.entity.HotDishes;
import com.fendu.entity.StapleFood;
import com.fendu.service.intf.ColdDishesService;
import com.fendu.service.intf.StapleFoodService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/StapleFood/")
public class StapleFoodController {
    @Autowired
    private StapleFoodService staleFoodService;
    //新增
    @PostMapping("save")
    public R save(StapleFood stapleFood){
        return staleFoodService.insertStapleFoodService(stapleFood);
    }
    //修改
    @PostMapping("update")
    public R update(StapleFood stapleFood){
        return staleFoodService.updateStapleFoodService(stapleFood);
    }

    //删除
    @PostMapping("delete")
    public R delete(int staple_id){
        return staleFoodService.deleteStapleFoodService(staple_id);
    }
    //查询
    @GetMapping("all")
    public PageVo<StapleFood> all(int page, int limit) {

        return staleFoodService.selectStapleFoodService(page, limit);
    }
}
