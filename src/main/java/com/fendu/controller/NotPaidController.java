package com.fendu.controller;

import com.fendu.entity.NotPaid;
import com.fendu.service.intf.NotPaidService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/NotPaid/")
public class NotPaidController {
    @Autowired
    private NotPaidService notPaidService;
    //增
    @PostMapping("add")
    public R add(NotPaid notPaid){return notPaidService.addService(notPaid);}
    //删
    @PostMapping("del")
    public R del(int id){return notPaidService.delService(id);}
    //改
    @PostMapping("upd")
    public R upd(NotPaid notPaid){return notPaidService.updService(notPaid);}
    //查
    @GetMapping("sel")
    public PageVo<NotPaid> sel(int page, int limit){return notPaidService.selService(page,limit);}
}
