package com.fendu.controller;

import com.fendu.entity.StockVegetable;
import com.fendu.service.intf.StockVegetableService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/StockVegetable/")
public class StockVegetableController {
    @Autowired
    private StockVegetableService stockVegetableService;

    //新增
    @PostMapping("saveVegetable")
    public R saveVegetable(StockVegetable stockVegetable){
        return stockVegetableService.insterSVegetable(stockVegetable);
    }

    //查询
    @GetMapping("selectVegetableAllList")
    public PageVo<StockVegetable> selectVegetableAllList(int page, int limit){
        return stockVegetableService.selectStockVegAll(page,limit);
    };

    //删除
    @PostMapping("deleteVegetableById")
    public R deleteVegetableById(int id){
        return stockVegetableService.deleteVegetableById(id);
    }

    //修改
    @PostMapping("updateVegetable")
    public R updateVegetable(StockVegetable stockVegetable){
        return stockVegetableService.updateVegetable(stockVegetable);
    }

}
