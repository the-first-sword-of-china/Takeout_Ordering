package com.fendu.controller;

import com.fendu.entity.DiningChair;
import com.fendu.entity.DiningTable;
import com.fendu.service.intf.DiningTableService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/diningTable/")
public class DiningTableController {
    @Autowired
    private DiningTableService diningTableService;

    //新增
    @PostMapping("saveTable")
    public R saveTable(DiningTable diningTable){
        return diningTableService.insterDiningTable(diningTable);
    }

    //查询
    @GetMapping("selectTableAllList")
    public PageVo<DiningTable> selectTableAllList(int page, int limit){
        return diningTableService.selectDiningTableService(page,limit);
    };

    //删除
    @PostMapping("deleteTableById")
    public R deleteTableById(int id){
        return diningTableService.deleteDiningTableById(id);
    }

    //修改
    @PostMapping("updateTable")
    public R updateTable(DiningTable diningTable){
        return diningTableService.updateDiningTable(diningTable);
    }
}
