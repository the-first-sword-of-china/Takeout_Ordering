package com.fendu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
//初始页，项目一启动，就访问login.html
@Controller()
public class IndexController {
    @GetMapping("/")
    public String index(){
        return "login.html";
    }
}
