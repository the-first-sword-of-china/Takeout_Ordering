package com.fendu.controller;


import com.fendu.entity.RateArea;
import com.fendu.entity.RatePerson;
import com.fendu.service.intf.RateAreaService;
import com.fendu.service.intf.RatePersonService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 锤子
 * @data 2021/10/7 18:01
 * @projectname Takeout_Ordering
 */

@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/area/")
public class RateAreaController {
    @Autowired
   private RateAreaService rateAreaService;
    //查询
    @GetMapping("all")
    public PageVo<RateArea> selectAllList(int page, int limit){

        return rateAreaService.selectAreaService(page, limit);
    }
    //新增
    @PostMapping("save")
    public R save(RateArea area){
        return rateAreaService.insertAreaService(area);
    }
    //删除
    @PostMapping("delete")
    public R delectAll(int area_id){

        return rateAreaService.delectAreaService(area_id);
    }
    //修改
    @PostMapping("update")
    public R update(RateArea rateArea){
        return rateAreaService.updateAreaService(rateArea);
    }

}
