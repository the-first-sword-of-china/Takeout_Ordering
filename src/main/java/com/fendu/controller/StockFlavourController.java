package com.fendu.controller;


import com.fendu.entity.StockBeverages;
import com.fendu.entity.StockFlavour;
import com.fendu.service.intf.StockBeveRagesService;
import com.fendu.service.intf.StockFlavourService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 锤子
 * @data 2021/10/7 18:01
 * @projectname Takeout_Ordering
 */
@RestController //等价于 @Controller+@ResponseBody
@RequestMapping("/api/flavo/")
public class StockFlavourController {
    @Autowired
    private StockFlavourService stockFlavourService;
    //查询
    @GetMapping("all")
    public PageVo<StockFlavour> selectAllList(int page, int limit){

        return stockFlavourService.selectFlavoService(page, limit);
    }
    //新增
    @PostMapping("save")
    public R save(StockFlavour stockFlavour){
        return stockFlavourService.insertFlavoService(stockFlavour);
    }
    //删除
    @PostMapping("delete")
    public R delectAll(int flavo_id){

        return stockFlavourService.delectFlavoService(flavo_id);
    }
    //修改
    @PostMapping("update")
    public R update(StockFlavour stockFlavour){
        return stockFlavourService.updateFlavoService(stockFlavour);
    }

}
