package com.fendu.controller;

import com.fendu.dao.SpecialtyDao;
import com.fendu.entity.Specialty;
import com.fendu.service.intf.SpecialtyService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/Specialty/")
public class SpecialtyController {
    @Autowired
    private SpecialtyService specialtyService;
    //新增
    @PostMapping("add")
    public R add(Specialty specialty){return specialtyService.add(specialty);}
    //查询
    @GetMapping("all")
    public PageVo<Specialty> page(int page, int limit){ return specialtyService.selectAll(page, limit); }
    //修改
    @PostMapping("edit")
    public R edit(Specialty specialty){
        return specialtyService.edit(specialty);
    }
    //删除
    @PostMapping("dele")
    public R dele(int id){return specialtyService.dele(id);}

}
