package com.fendu.util;
////////////////////////////////////////////////////////////////////
//                          _ooOoo_                               //
//                         o8888888o                              //
//                         88" . "88                              //
//                         (| ^_^ |)                              //
//                         O\  =  /O                              //
//                      ____/`---'\____                           //
//                    .'  \\|     |//  `.                         //
//                   /  \\|||  :  |||//  \                        //
//                  /  _||||| -:- |||||-  \                       //
//                  |   | \\\  -  /// |   |                       //
//                  | \_|  ''\---/''  |   |                       //
//                  \  .-\__  `-`  ___/-. /                       //
//                ___`. .'  /--.--\  `. . ___                     //
//              ."" '<  `.___\_<|>_/___.'  >'"".                  //
//            | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
//            \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
//      ========`-.____`-.___\_____/___.-`____.-'========         //
//                           `=---='                              //
//      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
//             佛祖保佑       永不宕机      永无BUG               //
////////////////////////////////////////////////////////////////////

import java.lang.reflect.Field;

/**
 * @description:
 * @author: Feri(邢朋辉)
 * @time: 2021/10/8 11:21
 */
public class BeanUtil {
    /**
     * 实现2个类的属性值的赋值
     * @param clz  结果类的字节码对象
     * @param obj 提供数据的对象
     * @param fields 提供数据的对象的属性的数组*/
    public static <T> T copyProperty(Class<T> clz, Object obj, Field[] fields){
        try {
            //1.创建结果类对象
            T result=clz.newInstance();
            //2.遍历属性 获取属性的值和名称
            for(Field f:fields){
                //设置不校验访问修饰符
                f.setAccessible(true);
                //获取属性的名称
                String name=f.getName();
                //获取类中指定属性名的属性对象
                Field field=clz.getDeclaredField(name);
                //校验时候存在
                if(field!=null){

                    field.setAccessible(true);
                    //赋值
                    field.set(result,f.get(obj));
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}