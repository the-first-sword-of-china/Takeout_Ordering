package com.fendu.config;

public class SystemContant {
    //接口的返回码
    public static final int R_OK=200;
    public static final int R_FAIL=400;

    //账号的状态标记
    //账号的标记位
    public static final int ADMIN_STATE_OK=1;//正常
    public static final int ADMIN_STATE_TEMP=2;//临时，待定
    public static final int ADMIN_STATE_INVA=3;//离职

    //AES的密钥
    public static final String PASS_KEY="YVXGC288ZZX3JCwl+npO/g==";

    //标记分页结果的状态码
    public static final int PAGE_R=0;
}
