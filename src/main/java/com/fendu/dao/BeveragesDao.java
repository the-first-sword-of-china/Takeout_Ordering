package com.fendu.dao;
import com.fendu.entity.Beverages;
import java.util.List;

/**
 * @author 锤子
 * @data 2021/10/5 23:56
 * @projectname LoginDemo
 */
public interface BeveragesDao {
    //新增
    int insterBeverDao(Beverages beverages);
    //查询
    List<Beverages> selectBeverDao();
}
