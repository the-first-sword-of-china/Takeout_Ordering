package com.fendu.dao;

import com.fendu.entity.Table;

import java.util.List;

public interface TableDao {
    //新增
    int inertTableDao(Table table);
    //查询
    List<Table> selectTableDao();
    //查询2.0
    List<Table> selectTableDao2(Table table);
    //修改
    int updateTableDao(Table table);
    //删除
    int deleteTableDao(int table_id);

    int insertBatch(List<Table> list);
}
