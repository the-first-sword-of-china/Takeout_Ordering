package com.fendu.dao;

import com.fendu.entity.BranchFinanMonth;


import java.util.List;

public interface BranchFinanMonthDao {
    //增
    int addDao(BranchFinanMonth branchFinanMonth);
    //删
    int delDao(int id);
    //改
    int updDao(BranchFinanMonth branchFinanMonth);
    //查
    List<BranchFinanMonth> selDao();
}
