package com.fendu.dao;

import com.fendu.entity.BranchFinanWeek;

import java.util.List;

public interface BranchFinanWeekDao {
    //增
    int addDao(BranchFinanWeek branchFinanWeek);
    //删
    int delDao(int id);
    //改
    int updDao(BranchFinanWeek branchFinanWeek);
    //查
    List<BranchFinanWeek>selDao();
}
