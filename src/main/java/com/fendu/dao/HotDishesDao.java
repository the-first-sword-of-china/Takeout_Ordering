package com.fendu.dao;

import com.fendu.entity.HotDishes;
import com.fendu.vo.R;

import java.util.List;

/**
 * @author 锤子
 * @data 2021/10/5 23:56
 * @projectname LoginDemo
 */
public interface HotDishesDao {
    //新增
    int insterHotDao(HotDishes hotDishes);
    //查询
    List<HotDishes> selectHotDao();
    //修改
    int updateHotDao(HotDishes hotDishes);
    //删除
    int deleteHotDao(int hot_id);
}
