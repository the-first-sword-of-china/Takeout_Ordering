package com.fendu.dao;

import com.fendu.entity.FinaMonth;
import com.fendu.entity.FinaYear;

import java.util.List;

public interface FinanYearDao {
    //查询
    List<FinaYear> selectFinaYearDao();
    //新增
    int insertFinaYearDao(FinaYear finaYear);

    //修改
    int updateFinaYearDao(FinaYear finaYear);
    //删除
    int deleteFinaYearDao(int fina_year_id);
}
