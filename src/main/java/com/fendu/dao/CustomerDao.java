package com.fendu.dao;

import com.fendu.entity.Customer;
import com.fendu.entity.Table;

import java.util.List;

public interface CustomerDao {
    //新增
    int insertCustomerDao(Customer customer);
    //查询
    List<Customer> selectCustomerDao();
    //修改
    int updateCustomerDao(Customer customer);
    //删除
    int deleteCustomerDao(int cust_id);
}
