package com.fendu.dao;

import com.fendu.entity.StapleFood;

import java.util.List;

public interface StapleFoodDao {
    //查询并展出
    List<StapleFood> selectStapleFoodDao();
    //新增
    int insterStapleFoodDao(StapleFood stapleFood);
    //修改
    int updateStapleFoodDao(StapleFood stapleFood);
    //删除
    int deleteStapleFoodDao(int staple_id);
}
