package com.fendu.dao;

import com.fendu.entity.StockBeverages;
import com.fendu.entity.StockFlavour;

import java.util.List;

/**
 * @author 锤子
 * @data 2021/10/8 17:09
 * @projectname Takeout_Ordering02
 */
public interface StockFlavourDao {


    List<StockFlavour> selectFlavotDao();

    int  insterFlavoDao(StockFlavour stockFlavour);

    int deleFlavoDao(int flavo_id);

    int updateFlavotDao(StockFlavour stockFlavour);
}
