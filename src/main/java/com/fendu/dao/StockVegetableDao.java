package com.fendu.dao;

import com.fendu.entity.StockVegetable;

import java.util.List;

public interface StockVegetableDao {
    //新增
    int insterVegetable(StockVegetable stockVegetable);

    //查询
    List<StockVegetable> selectVegetableAll();

    //删除
    int deleteVegetableById(int id);

    //修改
    int updateStockVegetable(StockVegetable stockVegetable);
}
