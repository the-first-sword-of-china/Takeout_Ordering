package com.fendu.dao;

import com.fendu.entity.StoreRate;

import java.util.List;

public interface StoreRateDao {
    //增
    int add(StoreRate storeRate);
    //删
    int del(int id);
    //改
    int upd(StoreRate storeRate);
    //查
    List<StoreRate>sel();
}
