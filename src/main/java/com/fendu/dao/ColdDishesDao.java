package com.fendu.dao;

import com.fendu.entity.ColdDishes;

import java.util.List;

/**
 * @author 锤子
 * @data 2021/10/5 12:35
 * @projectname Takeout_Ordering
 */
public interface ColdDishesDao {
    //新增
    int insterClodDao(ColdDishes coldDishes);

    //查询
    List<ColdDishes> selectColdDao();

    //删除
    int deleteColdById(int id);

    //修改
    int updateColdById(ColdDishes coldDishes);

}
