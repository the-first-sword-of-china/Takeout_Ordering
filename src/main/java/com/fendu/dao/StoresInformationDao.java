package com.fendu.dao;

import com.fendu.entity.FinaWeek;
import com.fendu.entity.StoresInformation;

import java.util.List;

public interface StoresInformationDao {
    //查询
    List<StoresInformation> selectStoresInformationDao();
    //新增
    int insertStoresInformationDao(StoresInformation storesInformation);

    //修改
    int updateStoresInformationDao(StoresInformation storesInformation);
    //删除
    int deleteStoresInformationDao(int stores_id);
}
