package com.fendu.dao;

import com.fendu.entity.Specialty;

import java.util.List;

public interface SpecialtyDao {
    //新增
    int addDao(Specialty specialty);
    //查询
    List<Specialty> selectAllDao();
    //修改
    int  update(Specialty specialty);
    //删除
    int  delete(int id);
}

