package com.fendu.dao;

import com.fendu.entity.HotDishes;
import com.fendu.entity.Staff;

import java.util.List;

public interface StaffDao {
    //查询
    List<Staff> selectStaffDao();
    //新增
    int insertStaffDao(Staff staff);

    //修改
    int updateStaffDao(Staff staff);
    //删除
    int deleteStaffDao(int staff_id);
}
