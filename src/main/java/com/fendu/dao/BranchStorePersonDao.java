package com.fendu.dao;



import com.fendu.entity.BranchStorePerson;

import java.util.List;

public interface BranchStorePersonDao {
    int insertDao(BranchStorePerson branchStorePerson);
    List<BranchStorePerson> selectDao();
    int updateDao(BranchStorePerson branchStorePerson);
    int deleteDao(int storePerson_id);
}
