package com.fendu.dao;

import com.fendu.entity.HotDishes;
import com.fendu.entity.Meat;

import java.util.List;

/**
 * @author 锤子
 * @data 2021/10/5 23:56
 * @projectname LoginDemo
 */
public interface MeatDao {
//    //新增
//    int insterHotDao(HotDishes hotDishes);
//    //查询
//    List<HotDishes> selectHotDao();
//
//    int updateHotDao(HotDishes hotDishes);
//
//    int deleteHotDao(int hot_id);
   //查询数据库所有数据并展示
    List<Meat> selectMeatDao();

    //添加
    int insterMeatDao(Meat meat);
    //删除
    int deleMeatDao(int meat_id);

    int updateMeatDao(Meat meat);
}
