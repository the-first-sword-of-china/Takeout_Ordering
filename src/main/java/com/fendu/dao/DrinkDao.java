package com.fendu.dao;

import com.fendu.entity.Drink;

import java.util.List;

public interface DrinkDao {
    //查询
    Drink selectBybeve_id(int beve_id);
    //增加
    int save(Drink drink);

    List<Drink> selectAll();

    int delete(int beve_numid);

    int update(Drink drink);
}
