package com.fendu.dao;

import com.fendu.entity.FinaWeek;

import java.util.List;

public interface FinaWeekDao {
    //查询
    List<FinaWeek> selectFinaWeekDao();
    //新增
    int insertFinaWeekDao(FinaWeek finaWeek);

    //修改
    int updateFinaWeekDao(FinaWeek finaWeek);
    //删除
    int deleteFinaWeekDao(int fina_week_id);
}
