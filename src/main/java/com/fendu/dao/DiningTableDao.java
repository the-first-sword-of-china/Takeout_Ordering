package com.fendu.dao;

import com.fendu.entity.DiningTable;
import java.util.List;

public interface DiningTableDao {
    //新增
    int insterDiningTable(DiningTable diningTable);

    //查询
    List<DiningTable> selectDiningTableAll();

    //删除
    int deleteDiningTableById(int id);

    //修改
    int updateTableById(DiningTable diningTable);
}
