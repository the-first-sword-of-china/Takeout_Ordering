package com.fendu.dao;

import com.fendu.entity.DiningTableWare;
import java.util.List;

public interface DiningTableWareDao {
    //新增
    int insterDiningWare(DiningTableWare diningTableWare);

    //查询
    List<DiningTableWare> selectWareAll();

    //删除
    int deleteTableWareById(int id);

    //修改
    int updateWareById(DiningTableWare diningTableWare);
}
