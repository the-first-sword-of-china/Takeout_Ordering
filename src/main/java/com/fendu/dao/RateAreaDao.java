package com.fendu.dao;

import com.fendu.entity.RateArea;

import java.util.List;

/**
 * @author 锤子
 * @data 2021/10/9 10:34
 * @projectname Takeout_Ordering02
 */
public interface RateAreaDao {
    List<RateArea> selectAreaDao();

    int  insterAreaDao(RateArea rateArea);

    int  deleAreaDao(int area_id);

    int  updateAreaDao(RateArea rateArea);
}
