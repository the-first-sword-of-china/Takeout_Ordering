package com.fendu.dao;

import com.fendu.entity.StockBeverages;

import java.util.List;

/**
 * @author 锤子
 * @data 2021/10/8 17:09
 * @projectname Takeout_Ordering02
 */
public interface StockBeveRagesDao {

    List<StockBeverages> selectBevetDao();

    int insterBeveDao(StockBeverages stockBeverages);

    int  deleBeveDao(int beve_id);

    int  updateBevetDao(StockBeverages stockBeverages);

}
