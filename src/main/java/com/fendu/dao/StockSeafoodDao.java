package com.fendu.dao;

import com.fendu.entity.StockSeafood;

import java.util.List;

public interface StockSeafoodDao {
    //新增
    int insterSeafood(StockSeafood stockSeafood);

    //查询
    List<StockSeafood> selectSeafoodAll();

    //删除
    int deleteSeafoodById(int id);

    //修改
    int updateSeafoodById(StockSeafood stockSeafood);
}
