package com.fendu.dao;

import com.fendu.entity.FinaMonth;

import java.util.List;

public interface FinanMonthDao {
    //查询
    List<FinaMonth> selectFinaMonthDao();
    //新增
    int insertFinaMonthDao(FinaMonth finaMonth);

    //修改
    int updateFinaMonthDao(FinaMonth finaMonth);
    //删除
    int deleteFinaMonthDao(int fina_mon_id);
    List<FinaMonth> selectWhereDao(FinaMonth finaMonth);
    int insertBatch(List<FinaMonth> list);
}
