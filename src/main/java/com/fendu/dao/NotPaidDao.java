package com.fendu.dao;

import com.fendu.entity.NotPaid;


import java.util.List;

public interface NotPaidDao {
    //增
    int addDao(NotPaid havePaid);
    //删
    int delDao(int id);
    //改
    int updDao(NotPaid havePaid);
    //查
    List<NotPaid> selDao();
}

