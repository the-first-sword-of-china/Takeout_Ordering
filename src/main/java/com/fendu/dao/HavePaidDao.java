package com.fendu.dao;
import com.fendu.entity.HavePaid;
import com.fendu.vo.PageVo;

import java.util.List;


public interface HavePaidDao {
    //增
    int addDao(HavePaid havePaid);
    //删
    int delDao(int id);
    //改
    int updDao(HavePaid havePaid);
    //查
    List<HavePaid> selDao();
}
