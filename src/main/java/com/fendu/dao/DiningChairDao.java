package com.fendu.dao;

import com.fendu.entity.DiningChair;

import java.util.List;

public interface DiningChairDao {
    //新增
    int insterDiningChair(DiningChair diningChair);

    //查询
    List<DiningChair> selectDiningChairAll();

    //条件查询
    List<DiningChair> selectByCd(DiningChair diningChair);

    //删除
    int deleteDiningChairById(int id);

    //修改
    int updateDiningChairById(DiningChair diningChair);


}
