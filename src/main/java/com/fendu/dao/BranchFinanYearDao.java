package com.fendu.dao;

import com.fendu.entity.BranchFinanYear;

import java.util.List;

public interface BranchFinanYearDao {
    int insertBranchYearDao(BranchFinanYear branchFinanYear);
    List<BranchFinanYear> selectBranchYearDao();
    int updateBranchYearDao(BranchFinanYear branchFinanYear);
    int deleteBranchYearDao(int finanyear_id);
}
