package com.fendu.dao;

import com.fendu.entity.Meat;
import com.fendu.entity.RatePerson;

import java.util.List;

/**
 * @author 锤子
 * @data 2021/10/5 23:56
 * @projectname LoginDemo
 */
public interface RatePersonDao {


    List<RatePerson> selectPersonDao();

    int insterPersonDao(RatePerson ratePerson);

    int delePersonDao(int rate_id);

    int updatePersonDao(RatePerson ratePerson);
}
