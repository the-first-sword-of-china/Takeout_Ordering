package com.fendu.dao;

import com.fendu.entity.Admin;
import org.apache.ibatis.annotations.Param;


public interface AdminDao {
    //登录 ，查询返回给业务层一个Admin对象 查询条件是是他的name
    Admin selectByName(String adminName);
    //修改管理员信息 未实现
    int editAdminDao(Admin admin);

    //改密码 未实现
    int updatePasswordDao(@Param("adminId") int adminId, @Param("adminPassword")String adminPassword);
}
