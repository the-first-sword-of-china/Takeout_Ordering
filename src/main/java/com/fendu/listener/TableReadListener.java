package com.fendu.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.fendu.dao.TableDao;
import com.fendu.dto.TableExcelDto;
import com.fendu.entity.Table;
import com.fendu.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TableReadListener extends AnalysisEventListener<TableExcelDto> {
    @Autowired
    private TableDao tableDao;

    private List<Table> list = new ArrayList<>();

    //获取读取到的每一行对应的对象
    @Override
    public void invoke(TableExcelDto tableExcelDto, AnalysisContext analysisContext) {
        //实现属性的赋值
        Table table = BeanUtil.copyProperty(Table.class, tableExcelDto, tableExcelDto.getClass().getDeclaredFields());
        /*System.out.println(table);*/
        list.add(table);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

        System.err.println(list);
        //将读取的数据一次性的存储到数据库
        int r = tableDao.insertBatch(list);
//        System.out.println(r);
    }
}
