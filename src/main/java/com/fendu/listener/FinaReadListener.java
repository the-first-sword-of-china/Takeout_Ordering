package com.fendu.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.fendu.dao.FinanMonthDao;
import com.fendu.dto.FinaMonthExcelDto;
import com.fendu.entity.FinaMonth;
import com.fendu.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FinaReadListener extends AnalysisEventListener<FinaMonthExcelDto> {
    @Autowired
    private FinanMonthDao finanMonthDao;
    private List<FinaMonth> list=new ArrayList<>();
    @Override
    public void invoke(FinaMonthExcelDto finaMonthExcelDto, AnalysisContext analysisContext) {
        FinaMonth finaMonth=BeanUtil.copyProperty(FinaMonth.class,finaMonthExcelDto,finaMonthExcelDto.getClass().getDeclaredFields());
        list.add(finaMonth);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        System.out.println(list);
        int r=finanMonthDao.insertBatch(list);

    }
}
