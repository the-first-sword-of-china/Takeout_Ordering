package com.fendu.test;

import com.alibaba.excel.EasyExcel;
import com.fendu.dto.TableExcelDto;
import com.fendu.entity.Table;
import com.fendu.listener.TableReadListener;
import com.fendu.util.BeanUtil;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class Excel_Test {
    @Test
    public void t1(){
        EasyExcel.read("D://table.xlsx", TableExcelDto.class,new TableReadListener()).sheet().doRead();
    }
    @Test
    public void t2(){
        TableExcelDto tableExcelDto=new TableExcelDto();
        tableExcelDto.setTable_type(1);
        tableExcelDto.setTable_state(1);
        tableExcelDto.setTable_numid("111");
        Table table = BeanUtil.copyProperty(Table.class, tableExcelDto, tableExcelDto.getClass().getDeclaredFields());
        System.out.println(table);
    }
    @Test
    public void t3(){
        //准备数据
        List<TableExcelDto> list = new ArrayList<>();
        for(int i=1000;i<1100;i++){
            TableExcelDto tableExcelDto = new TableExcelDto();
            tableExcelDto.setTable_type(1);
            tableExcelDto.setTable_state(2);
            tableExcelDto.setTable_numid("批量--"+i);
            list.add(tableExcelDto);
        }
        //写出
        EasyExcel.write("ax.xlsx",TableExcelDto.class).sheet().doWrite(list);
    }

}
