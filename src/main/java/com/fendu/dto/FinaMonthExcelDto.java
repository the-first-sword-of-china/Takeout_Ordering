package com.fendu.dto;


import com.alibaba.excel.annotation.ExcelProperty;
import jdk.nashorn.internal.objects.annotations.Property;
import lombok.Data;

@Data
public class FinaMonthExcelDto {
    @ExcelProperty("支出")
    private double fina_mon_extend;
    @ExcelProperty("收入")
    private double fina_mon_income;

    @ExcelProperty("利润")
    private double fina_mon_profit;
}
