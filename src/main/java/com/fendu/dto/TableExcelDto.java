package com.fendu.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class TableExcelDto {
    @ExcelProperty("状态")
    private Integer table_state;
    @ExcelProperty("类型")
    private Integer table_type;
    @ExcelProperty("编号")
    private String table_numid;

}
