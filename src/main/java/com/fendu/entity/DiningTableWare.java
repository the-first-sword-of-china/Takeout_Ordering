package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class DiningTableWare {
    private Integer ware_id;//id
    private Integer ware_numid;//订单编号
    private Integer ware_amount;//数量
    private String ware_type;//类型
    private Integer ware_price;//单价
    private Integer ware_priceall;//总价
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date ware_day;//购买日期
    private String ware_name;//购买人
}
