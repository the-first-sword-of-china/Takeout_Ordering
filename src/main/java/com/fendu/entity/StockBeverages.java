package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author 锤子
 * @data 2021/10/8 16:55
 * @projectname Takeout_Ordering02
 */
@Data
public class StockBeverages {
    private Integer beve_id ;
    /** 订单编号 */
    private String beve_numid ;
    /** 购买地点 */
    private String beve_addres ;
    /** 单价 */
    private Double beve_price ;
    /** 总价 */
    private Double beve_priceall ;
    /** 类型 */
    private String beve_type ;
    /** 购买日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date beve_day ;
    /** 购买人 */
    private String beve_name ;
}
