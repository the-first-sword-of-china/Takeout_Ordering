package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class StockSeafood {
    private Integer sea_id;//序号
    private Integer sea_numid;//订单编号
    private String sea_address;//购买地点
    private Integer sea_price;//单价
    private Integer sea_priceall;//总价
    private String sea_type;//类型
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date sea_day;//购买日期
    private String sea_name;//购买人
}
