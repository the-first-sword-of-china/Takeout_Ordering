package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author 锤子
 * @data 2021/10/8 18:28
 * @projectname Takeout_Ordering02
 */
@Data
public class StockFlavour {
    private Integer flavo_id ;
    /** 订单编号 */
    private Integer flavo_numid ;
    /** 购买地点 */
    private Integer flavo_addres ;
    /** 单价 */
    private String flavo_price  ;
    /** 总价 */
    private Double flavo_priceall ;
    /** 类型 */
    private Double flavo_type ;
    /** 购买日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date flavo_day ;
    /** 购买人 */
    private String flavo_name ;

}
