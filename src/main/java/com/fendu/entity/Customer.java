package com.fendu.entity;

import lombok.Data;

@Data
public class Customer {
    private int cust_id ;
    private int cust_numid;
    private String cust_name;
    private String cust_address;
    private String cust_phone;
    private String cust_idcard;
    private int cust_sex;
    private int cust_type;
}
