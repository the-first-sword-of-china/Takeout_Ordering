package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class HavePaid {
    /** 主键 */
    private Integer have_paid_id ;
    /** 菜品 */
    private String have_paid_cui ;
    /** 菜名 */
    private String have_paid_dis ;
    /** 价格 */
    private Double have_paid_pri ;
    /** 以收金额 */
    private Double have_paid_mon ;
    /** 下单时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date have_paid_dis_ort ;
    /** 支付时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date have_paid_dis_pat ;
    /** 订单编号 */
    private String have_paid_dis_orm ;

}
