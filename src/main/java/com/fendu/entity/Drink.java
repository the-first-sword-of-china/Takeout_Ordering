package com.fendu.entity;

import lombok.Data;

@Data
public class Drink {
    private int beve_id;
    private int beve_numid;
    private String beve_name;
    private double beve_price;
    private String beve_introduce;

}
