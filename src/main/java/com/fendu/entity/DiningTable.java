package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class DiningTable {
    private Integer table_id;//id
    private Integer table_numid;//订单编号
    private Integer table_amount;//数量
    private String table_type;//类型
    private Integer table_price;//单价
    private Integer table_priceall;//总价
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date table_day;//购买日期
    private String table_name;//购买人
}
