package com.fendu.entity;

import lombok.Data;

/**
 * @author 锤子
 * @data 2021/10/5 23:46
 * @projectname StudyStudyAdmin
 */
@Data
public class Beverages {
    private int id ;
    private int beve_numid;
    private String beve_name;
    private Double beve_price;
    private String beve_introduce;
}
