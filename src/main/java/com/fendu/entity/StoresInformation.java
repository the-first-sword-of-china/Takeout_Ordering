package com.fendu.entity;

import lombok.Data;

@Data
public class StoresInformation {
    private int stores_id;
    private int stores_numid;
    private String stores_name;
    private String stores_manager;
    private String stores_address;
    private String stores_phone;
}
