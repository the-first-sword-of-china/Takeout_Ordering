package com.fendu.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class BranchFinanYear {
    private int finanyear_id;
    private String stores_name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date fina_week_day;
    private double fina_week_expend;
    private double fina_week_income;
}
