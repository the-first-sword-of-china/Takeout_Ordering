package com.fendu.entity;

import lombok.Data;

@Data
public class Specialty {
    private Integer spec_id;
    private Integer spec_numid;
    private String spec_name;
    private double spec_price;
    private String spec_introduce;

}
