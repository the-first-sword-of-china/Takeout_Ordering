package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class StockVegetable {
    private Integer vegetable_id;//序号
    private Integer vegetable_numid;//订单编号
    private String vegetable_address;//购买地点
    private Integer vegetable_price;//单价
    private Integer vegetable_priceall;//总价
    private String vegetable_type;//类型
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date vegetable_day;//购买日期
    private String vegetable_name;//购买人
}
