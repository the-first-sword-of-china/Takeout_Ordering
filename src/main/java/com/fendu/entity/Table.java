package com.fendu.entity;

import lombok.Data;

@Data
public class Table {
    private Integer table_id ;
    private String table_numid;

    private Integer table_type;
    private Integer table_state;
}
