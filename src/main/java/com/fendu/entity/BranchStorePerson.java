package com.fendu.entity;

import lombok.Data;

@Data
public class BranchStorePerson {
    private int storePerson_id;
    private int stores_numid;
    private int staf_numid;
    private String staf_name;
    private String staf_sex;

    private String staf_office;

}
