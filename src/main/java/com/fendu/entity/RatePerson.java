package com.fendu.entity;

import lombok.Data;

/**
 * @author 锤子
 * @data 2021/10/8 20:52
 * @projectname Takeout_Ordering02
 */
@Data
public class RatePerson {
    private Integer rate_id ;
    /** 评级人姓名 */
    private String rate_name;
    /** 评级人性别 */
    private String rate_sex ;
    /** 评级人编号 */
    private String rate_numid ;
    /** 评级人电话 */
    private String rate_phone ;
    /** 评级人住址 */
    private String rate_address ;
    /** 评级人身份证 */
    private String rate_idcard ;

}
