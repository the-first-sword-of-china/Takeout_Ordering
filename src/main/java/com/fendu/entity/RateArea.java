package com.fendu.entity;

import lombok.Data;

/**
 * @author 锤子
 * @data 2021/10/9 10:13
 * @projectname Takeout_Ordering02
 */
@Data
public class RateArea {
    private Integer area_id ;
    /** 厨房 */
    private String area_kitchen ;
    /** 前台 */
    private String area_reception ;
    /** 就餐区 */
    private String area_eat ;
    /** 卫生间 */
    private String area_toilet ;
    /** 总体评级 */
    private String rate_all ;


}
