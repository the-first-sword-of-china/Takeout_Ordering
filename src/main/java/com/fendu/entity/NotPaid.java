package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class NotPaid {
    /** 主键 */
    private Integer not_paid_id ;
    /** 菜品 */
    private String not_paid_cui ;
    /** 菜名 */
    private String not_paid_dis ;
    /** 价格 */
    private Double not_paid_pri ;
    /** 以收金额 */
    private Double not_paid_mon ;
    /** 下单时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date not_paid_dis_pat ;
    /** 订单编号 */
    private String not_paid_dis_orm ;
}
