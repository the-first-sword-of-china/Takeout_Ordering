package com.fendu.entity;

import lombok.Data;

@Data
public class StapleFood {
    private Integer staple_id;
    private Integer staple_numid;
    private String staple_name;
    private double staple_price;
    private String staple_introduce;

}
