package com.fendu.entity;

import lombok.Data;

@Data
public class Staff {
    private int staff_id ;
    private int staff_numid;
    private String staff_name;
    private String staff_address;
    private String staff_phone;
    private String staff_idcard;
    private int staff_sex;
    private int staff_type;
}
