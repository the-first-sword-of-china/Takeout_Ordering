package com.fendu.entity;

import lombok.Data;

@Data
public class ColdDishes {

    private Integer cold_id;
    private Integer cold_numid;
    private String cold_name;
    private Double cold_price;
    private String cold_introduce;



}
