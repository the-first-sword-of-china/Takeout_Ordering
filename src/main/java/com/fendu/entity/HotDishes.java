package com.fendu.entity;

import lombok.Data;

/**
 * @author 锤子
 * @data 2021/10/5 23:46
 * @projectname StudyStudyAdmin
 */
@Data
public class HotDishes {
    private int hot_id ;
    private int hot_numid;
    private String hot_name;
    private Double hot_price;
    private String hot_introduce;
}
