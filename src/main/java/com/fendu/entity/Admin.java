package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class Admin {
    /** ID */
    private int adminId ;
    /** 姓名 */
    private String adminName ;
    /** 密码 */
    private String adminPassword ;
    /** 管理员状态 */
    private int adminState ;
    //管理员的手机
    private String adminPhone;
    //管理员的邮箱
    private String adminEmail;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date adminCtime;


}
