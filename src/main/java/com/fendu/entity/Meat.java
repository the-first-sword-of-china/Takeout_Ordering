package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author 锤子
 * @data 2021/10/7 18:05
 * @projectname Takeout_Ordering
 */
@Data
public class Meat {
    /** 主键 */

    private Integer meat_id ;
    /** 订单编号 */
    private Integer meat_numid ;
    /** 购买地点 */
    private String meat_address ;
    /** 单价 */
    private Double meat_price ;
    /** 总价 */
    private Double meat_priceall ;
    /** 类型 */
    private String meat_type ;
    /** 购买日期 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date meat_day ;
    /** 购买人 */
    private String meat_name ;

}
