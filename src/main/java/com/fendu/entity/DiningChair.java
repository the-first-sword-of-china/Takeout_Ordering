package com.fendu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class DiningChair {
    private Integer chair_id;//id
    private String chair_numid;//订单编号
    private Integer chair_amount;//数量
    private String chair_type;//类型
    private Integer chair_price;//单价
    private Integer chair_priceall;//总价
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date chair_day;//购买日期
    private String chair_name;//购买人
}
