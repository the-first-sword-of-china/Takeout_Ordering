package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.CustomerDao;
import com.fendu.dao.TableDao;
import com.fendu.entity.Customer;
import com.fendu.entity.Table;
import com.fendu.service.intf.CustomerService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;

    //新增
    @Override
    public R insertCustomerService(Customer customer) {
        if (customerDao.insertCustomerDao(customer) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }
    //查询
    @Override
    public PageVo<Customer> selectCustomerService(int page, int limit) {

        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<Customer> pageInfo=new PageInfo<>(customerDao.selectCustomerDao());

        return new PageVo<Customer>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }
    //修改
    @Override
    public R updateCustomerService(Customer customer) {
        if (customerDao.updateCustomerDao(customer)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
    //删除
    @Override
    public R deleteCustomerService(int cust_id) {
        if(customerDao.deleteCustomerDao(cust_id)>0){
            return R.ok(null);
        }else {
            return R.fail("删除失败,请咨询CEO");
        }
    }
}
