package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.FinaWeekDao;
import com.fendu.entity.FinaWeek;
import com.fendu.service.intf.FinaWeekService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FinaWeekServiceImpl implements FinaWeekService {
    @Autowired
    private FinaWeekDao finaWeekDao;
    //查询
    @Override
    public PageVo<FinaWeek> selectFinaWeekService(int page, int limit) {
        PageHelper.startPage(page,limit);
        PageInfo<FinaWeek> pageInfo = new PageInfo<>(finaWeekDao.selectFinaWeekDao());
        return new PageVo<FinaWeek>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }
    //新增
    @Override
    public R insertFinaWeekService(FinaWeek finaWeek) {
        if (finaWeekDao.insertFinaWeekDao(finaWeek) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    //修改
    @Override
    public R updateFinaWeekService(FinaWeek finaWeek) {
        if (finaWeekDao.updateFinaWeekDao(finaWeek)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
    //删除
    @Override
    public R deleteFinaWeekService(int fina_week_id) {
        if(finaWeekDao.deleteFinaWeekDao(fina_week_id)>0){
            return R.ok(null);
        }else {
            return R.fail("删除失败,请咨询CEO");
        }
    }
}
