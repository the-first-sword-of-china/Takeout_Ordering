package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StockStapleFoodDao;
import com.fendu.entity.StockStapleFood;
import com.fendu.service.intf.StockStapleService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockStapleFoodServiceImpl implements StockStapleService {
    @Autowired
    private StockStapleFoodDao stockStapleFoodDao;


    @Override
    public R insertStockStapleService(StockStapleFood stockStapleFood) {
        if (stockStapleFoodDao.insterStockStapleFoodDao(stockStapleFood) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public PageVo<StockStapleFood> selectStockStapleService(int page, int limit) {
        //开启分页
        PageHelper.startPage(page,limit);
        PageInfo<StockStapleFood> pageInfo = new PageInfo<>(stockStapleFoodDao.selectStockStapleFoodDao());

        return new PageVo<StockStapleFood>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public R updateStockStapleService(StockStapleFood stockStapleFood) {
        if (stockStapleFoodDao.updateStockStapleFoodDao(stockStapleFood)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    @Override
    public R deleteStockStapleService(int staple_id) {
        if(stockStapleFoodDao.deleteStockStapleFoodDao(staple_id)>0){
            return R.ok(null);
        }else {
            return R.fail("删除失败,请咨询CEO");
        }
    }
}

