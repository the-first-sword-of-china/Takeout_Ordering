package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.HotDishesDao;
import com.fendu.dao.MeatDao;
import com.fendu.entity.HotDishes;
import com.fendu.entity.Meat;
import com.fendu.service.intf.HotDishesService;
import com.fendu.service.intf.MeatService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 锤子
 * @data 2021/10/5 23:55
 * @projectname LoginDemo
 */
@Service

public class MeatServiceImpl implements MeatService {
    @Autowired
    private MeatDao  meatDao;
    @Override
    public PageVo<Meat> selectMeatService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<Meat> pageInfo=new PageInfo<>(meatDao.selectMeatDao());

        return new PageVo<Meat>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());

    }

    @Override
    public R insertMeatService(Meat meat) {
        if (meatDao.insterMeatDao(meat) > 0) {
            return R.ok(null);
        }else {
            return  R.fail();
        }
    }

    @Override
    public R delectMeatService(int meat_id) {
        if(meatDao.deleMeatDao(meat_id) > 0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
    //修改
    @Override
    public R updateMeatService(Meat meat) {
        if (meatDao.updateMeatDao(meat)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }


//    @Autowired
//    private HotDishesDao hotDishesDao;
//    @Override
//    public R insertHotService(HotDishes hotDishes) {
//        if (hotDishesDao.insterHotDao(hotDishes) > 0) {
//            return R.ok(null);
//        } else {
//            return R.fail();
//        }
//    }
//
//    @Override
//    public PageVo<HotDishes> selectHotService(int page,int limit) {
//        //1.开启分页插件
//        PageHelper.startPage(page, limit);
//        //2.获取分页的查询结果
//        PageInfo<HotDishes> pageInfo=new PageInfo<>(hotDishesDao.selectHotDao());
//
//        return new PageVo<HotDishes>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
//    }
//    //修改
//    @Override
//    public R updateHotService(HotDishes hotDishes) {
//        if (hotDishesDao.updateHotDao(hotDishes)>0){
//            return R.ok(null);
//        }else {
//            return R.fail();
//        }
//    }
//
//    @Override
//    public R deleteHotService(int hot_id) {
//        if(hotDishesDao.deleteHotDao(hot_id)>0){
//            return R.ok(null);
//        }else {
//            return R.fail("删除失败,请咨询CEO");
//        }
//    }
}
