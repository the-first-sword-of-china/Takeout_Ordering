package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.BranchFinanMonthDao;
import com.fendu.entity.BranchFinanMonth;
import com.fendu.service.intf.BranchFinanMonthService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("BranchFinanMonthService")
public class BranchFinanMonthServiceImpl implements BranchFinanMonthService {
    @Autowired
    private BranchFinanMonthDao branchFinanMonthDao;
    //增
    @Override
    public R addService(BranchFinanMonth branchFinanMonth) {
        if (branchFinanMonthDao.addDao(branchFinanMonth)>0){
            return R.ok(null);
        }
        return R.fail();
    }
    //删
    @Override
    public R delService(int id) {
        if (branchFinanMonthDao.delDao(id)>0){
            return R.ok(null);
        }
        return R.fail();
    }
    //改
    @Override
    public R updService(BranchFinanMonth branchFinanMonth) {
        if (branchFinanMonthDao.updDao(branchFinanMonth)>0){
            return R.ok(null);
        }
        return R.fail();
    }
    //查
    @Override
    public PageVo<BranchFinanMonth> selService(int page, int limit) {
        //开启分页查询
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<BranchFinanMonth> pageInfo=new PageInfo<>(branchFinanMonthDao.selDao());
        return new PageVo<BranchFinanMonth>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }
}
