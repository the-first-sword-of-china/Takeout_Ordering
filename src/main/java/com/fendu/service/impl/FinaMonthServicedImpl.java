package com.fendu.service.impl;

import com.alibaba.excel.EasyExcel;
import com.fendu.bo.FinaMonthBo;
import com.fendu.config.SystemContant;
import com.fendu.dao.FinanMonthDao;
import com.fendu.dto.FinaMonthExcelDto;
import com.fendu.entity.FinaMonth;
import com.fendu.entity.FinaWeek;
import com.fendu.listener.FinaReadListener;
import com.fendu.service.intf.FinaMonthService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Service
public class FinaMonthServicedImpl implements FinaMonthService {
    @Autowired
    private FinanMonthDao finanMonthDao;
    @Autowired
    private FinaReadListener finaReadListener;
    @Override
    public PageVo<FinaMonth> selectFinaMonthService(int page, int limit) {
        PageHelper.startPage(page,limit);
        PageInfo<FinaMonth> pageInfo = new PageInfo<>(finanMonthDao.selectFinaMonthDao());
        return new PageVo<FinaMonth>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public PageVo<FinaMonth> selectFinaMonthWhereService(FinaMonthBo finaMonthBo) {
        if(finaMonthBo!=null){
            //1.组装查询条件
            FinaMonth finaMonth=new FinaMonth();
            if(finaMonthBo.getFina_mon_extend()>-1){
                finaMonth.setFina_mon_expend(finaMonthBo.getFina_mon_extend());
            }
            if(finaMonthBo.getFina_mon_income()>-1){
                finaMonth.setFina_mon_income(finaMonthBo.getFina_mon_income());
            }
            if(finaMonthBo.getFina_mon_profit()>-1){
                finaMonth.setFina_mon_profit(finaMonthBo.getFina_mon_profit());
            }
            //2.开启分页插件
            PageHelper.startPage(finaMonthBo.getPage(), finaMonthBo.getLimit());
            //3.获取分页的查询结果
            PageInfo<FinaMonth> pageInfo=new PageInfo<>(finanMonthDao.selectWhereDao(finaMonth));
            //4.返回结果
            return new PageVo<FinaMonth>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
        }
        return new PageVo<FinaMonth>(SystemContant.PAGE_R,"OK",0,null);
    }

    @Override
    public R insertFinaMonthService(FinaMonth finaMonth) {
        if (finanMonthDao.insertFinaMonthDao(finaMonth) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public R updateFinaMonthService(FinaMonth finaMonth) {
        if (finanMonthDao.updateFinaMonthDao(finaMonth) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public R deleteFinaMonthService(int fina_mon_id) {
        if (finanMonthDao.deleteFinaMonthDao(fina_mon_id) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public R batchAdd(MultipartFile multipartFile) {
        if(!multipartFile.isEmpty()){
            //excel解析
            try {
                EasyExcel.read(multipartFile.getInputStream(), FinaMonthExcelDto.class,finaReadListener).sheet().doRead();
                return R.ok("OK");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return R.fail();
    }

    @Override
    public void exportExcel(FinaMonthBo finaMonthBo, HttpServletResponse httpServletResponse) {
        //1.设置响应消息头，目的：告诉请求方，这个是下载
        String fn=System.currentTimeMillis()+".xlsx";
        //设置响应头  告知浏览器，要以附件的形式保存内容   filename=浏览器显示的下载文件名
        httpServletResponse.setHeader("content-disposition","attachment;filename="+fn);

        //2.组装查询条件
        FinaMonth finaMonth=new FinaMonth();
        if(finaMonthBo.getFina_mon_extend()>-1){
            finaMonth.setFina_mon_expend(finaMonthBo.getFina_mon_extend());
        }
        if(finaMonthBo.getFina_mon_income()>-1){
            finaMonth.setFina_mon_income(finaMonthBo.getFina_mon_income());
        }
        if(finaMonthBo.getFina_mon_profit()>-1){
            finaMonth.setFina_mon_profit(finaMonthBo.getFina_mon_profit());
        }
        //3.获取要下载的数据
        List<FinaMonth> list=finanMonthDao.selectWhereDao(finaMonth);
        try {
            //4.生成Excel，并将数据写出到响应流中
            EasyExcel.write(httpServletResponse.getOutputStream(),FinaMonth.class).sheet().doWrite(list);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
