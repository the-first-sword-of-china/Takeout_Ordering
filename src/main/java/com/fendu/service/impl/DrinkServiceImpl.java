package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.DrinkDao;
import com.fendu.entity.Drink;
import com.fendu.service.intf.DrinkService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DrinkServiceImpl implements DrinkService {
    @Autowired
    private DrinkDao drinkDao;


    public Drink select(int spec_id) {
        return null;
    }

    public R save(Drink drink) {
        drink.setBeve_id(drink.getBeve_id());
        drink.setBeve_introduce(drink.getBeve_introduce());
        drink.setBeve_price(drink.getBeve_price());
        if(drinkDao.save(drink)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    public PageVo<Drink> queryPage(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<Drink> pageInfo=new PageInfo<>(drinkDao.selectAll());

        return new PageVo<Drink>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public R delete(int beve_numid) {
        if(drinkDao.delete(beve_numid)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    @Override
    public R update(Drink drink) {
        System.out.println(drink);
        if(drinkDao.update(drink)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
}
