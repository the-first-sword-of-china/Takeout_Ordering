package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StoreRateDao;
import com.fendu.entity.StoreRate;
import com.fendu.service.intf.StoreRateService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreRateServiceImpl implements StoreRateService {
    @Autowired
    private StoreRateDao storeRateDao;
    //增
    @Override
    public R addService(StoreRate storeRate) {
        if (storeRateDao.add(storeRate)>0){
            return R.ok("成功");
        }
        return R.fail();
    }
    //删
    @Override
    public R delService(int id) {
        if (storeRateDao.del(id)>0){
            return R.ok(null);
        }
        return R.fail();
    }
    //改
    @Override
    public R updService(StoreRate storeRate) {
        if (storeRateDao.upd(storeRate)>0){
            return R.ok("成功");
        }
        return R.fail();
    }
    //查
    @Override
    public PageVo<StoreRate> selService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        PageInfo<StoreRate> pageInfo = new PageInfo<>(storeRateDao.sel());
        return new PageVo<StoreRate>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }
}
