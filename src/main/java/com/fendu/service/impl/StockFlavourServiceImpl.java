package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StockBeveRagesDao;
import com.fendu.dao.StockFlavourDao;
import com.fendu.dao.StockStapleFoodDao;
import com.fendu.entity.StockBeverages;
import com.fendu.entity.StockFlavour;
import com.fendu.service.intf.StockBeveRagesService;
import com.fendu.service.intf.StockFlavourService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 锤子
 * @data 2021/10/8 17:08
 * @projectname Takeout_Ordering02
 */
@Service
public class StockFlavourServiceImpl implements StockFlavourService {
    @Autowired
  private StockFlavourDao stockFlavourDao;


    @Override
    public PageVo<StockFlavour> selectFlavoService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<StockFlavour> pageInfo=new PageInfo<>(stockFlavourDao.selectFlavotDao());

        return new PageVo<StockFlavour>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());

    }

    @Override
    public R insertFlavoService(StockFlavour stockFlavour) {
        if (stockFlavourDao.insterFlavoDao(stockFlavour) > 0) {
            return R.ok(null);
        }else {
            return  R.fail();
        }
    }

    @Override
    public R delectFlavoService(int flavo_id) {
        if(stockFlavourDao.deleFlavoDao(flavo_id) > 0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    @Override
    public R updateFlavoService(StockFlavour stockFlavour) {
        if (stockFlavourDao.updateFlavotDao(stockFlavour)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

}
