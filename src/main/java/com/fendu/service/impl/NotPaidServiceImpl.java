package com.fendu.service.impl;
import com.fendu.config.SystemContant;
import com.fendu.dao.NotPaidDao;
import com.fendu.entity.NotPaid;
import com.fendu.service.intf.NotPaidService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotPaidServiceImpl implements NotPaidService {
    @Autowired
    private NotPaidDao notPaidDao;
    @Override
    public R addService(NotPaid notPaid) {
        if (notPaidDao.addDao(notPaid)>0){
            return R.ok(null);
        }
        return R.fail();
    }

    @Override
    public R delService(int id) {
        if (notPaidDao.delDao(id)>0){
            return R.ok(null);
        }
        return R.fail();
    }


    @Override
    public R updService(NotPaid notPaid) {
        if (notPaidDao.updDao(notPaid)>0){
            return R.ok(null);
        }
        return R.fail();
    }

    @Override
    public PageVo<NotPaid> selService(int page, int limit) {
        //开启分页查询
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<NotPaid> pageInfo=new PageInfo<>(notPaidDao.selDao());
        return new PageVo<NotPaid>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }
}
