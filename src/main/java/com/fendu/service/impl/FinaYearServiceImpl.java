package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.FinanYearDao;
import com.fendu.entity.FinaMonth;
import com.fendu.entity.FinaYear;
import com.fendu.service.intf.FinaYearService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FinaYearServiceImpl implements FinaYearService {
    @Autowired
    private FinanYearDao finanYearDao;

    @Override
    public PageVo<FinaYear> selectFinaYearService(int page, int limit) {
        PageHelper.startPage(page,limit);
        PageInfo<FinaYear> pageInfo = new PageInfo<>(finanYearDao.selectFinaYearDao());
        return new PageVo<FinaYear>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public R insertFinaYearService(FinaYear finaYear) {
        if (finanYearDao.insertFinaYearDao(finaYear) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public R updateFinaYearService(FinaYear finaYear) {
        if (finanYearDao.updateFinaYearDao(finaYear) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public R deleteFinaYearService(int fina_year_id) {
        if (finanYearDao.deleteFinaYearDao(fina_year_id) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }
}
