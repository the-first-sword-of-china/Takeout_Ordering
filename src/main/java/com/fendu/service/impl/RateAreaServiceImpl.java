package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.RateAreaDao;
import com.fendu.entity.RateArea;
import com.fendu.service.intf.RateAreaService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 锤子
 * @data 2021/10/5 23:55
 * @projectname LoginDemo
 */
@Service

public class RateAreaServiceImpl implements RateAreaService {
    @Autowired
    private RateAreaDao rateAreaDao;
    @Override
    public PageVo<RateArea> selectAreaService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<RateArea> pageInfo=new PageInfo<>(rateAreaDao.selectAreaDao());

        return new PageVo<RateArea>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public R insertAreaService(RateArea rateArea) {
        if (rateAreaDao.insterAreaDao(rateArea) > 0) {
            return R.ok(null);
        }else {
            return  R.fail();
        }
    }

    @Override
    public R delectAreaService(int area_id) {
        if(rateAreaDao.deleAreaDao(area_id) > 0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    @Override
    public R updateAreaService(RateArea rateArea) {
        if (rateAreaDao.updateAreaDao(rateArea)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

}
