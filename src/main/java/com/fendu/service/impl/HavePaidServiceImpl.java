package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.HavePaidDao;
import com.fendu.entity.HavePaid;
import com.fendu.service.intf.HavePaidService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HavePaidServiceImpl implements HavePaidService {
    @Autowired
    private HavePaidDao havePaidDao;
    @Override
    public R addService(HavePaid havePaid) {
        if (havePaidDao.addDao(havePaid)>0){
            return R.ok(null);
        }
        return R.fail();
    }

    @Override
    public R delService(int id) {
        if (havePaidDao.delDao(id)>0){
            return R.ok(null);
        }
        return R.fail();
    }

    @Override
    public R updService(HavePaid havePaid) {
        if (havePaidDao.updDao(havePaid)>0){
            return R.ok(null);
        }
        return R.fail();
    }

    @Override
    public PageVo<HavePaid> selService(int page, int limit) {
        //开启分页查询
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<HavePaid> pageInfo=new PageInfo<>(havePaidDao.selDao());
        return new PageVo<HavePaid>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }
}
