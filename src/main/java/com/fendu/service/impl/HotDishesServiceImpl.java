package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.HotDishesDao;
import com.fendu.entity.HotDishes;
import com.fendu.service.intf.HotDishesService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 锤子
 * @data 2021/10/5 23:55
 * @projectname LoginDemo
 */
@Service

public class HotDishesServiceImpl implements HotDishesService {
    @Autowired
    private HotDishesDao hotDishesDao;
    //添加
    @Override
    public R insertHotService(HotDishes hotDishes) {
        if (hotDishesDao.insterHotDao(hotDishes) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    //展示
    @Override
    public PageVo<HotDishes> selectHotService(int page,int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<HotDishes> pageInfo=new PageInfo<>(hotDishesDao.selectHotDao());

        return new PageVo<HotDishes>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }
    //修改
    @Override
    public R updateHotService(HotDishes hotDishes) {
        if (hotDishesDao.updateHotDao(hotDishes)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    //删除
    @Override
    public R deleteHotService(int hot_id) {
        if(hotDishesDao.deleteHotDao(hot_id)>0){
            return R.ok(null);
        }else {
            return R.fail("删除失败,请咨询CEO");
        }
    }
}
