package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StaffDao;
import com.fendu.entity.Customer;
import com.fendu.entity.Staff;
import com.fendu.service.intf.StaffService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StaffServiceImpl implements StaffService {
    @Autowired
    private StaffDao staffDao;
    //查询
    @Override
    public PageVo<Staff> selectStaffService(int page, int limit) {
        PageHelper.startPage(page,limit);
        PageInfo<Staff> pageInfo = new PageInfo<>(staffDao.selectStaffDao());
        return new PageVo<Staff>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }
    //新增
    @Override
    public R insertStaffService(Staff staff) {
        if (staffDao.insertStaffDao(staff) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    //修改
    @Override
    public R updateStaffService(Staff staff) {
        if (staffDao.updateStaffDao(staff)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
    //删除
    @Override
    public R deleteStaffService(int staff_id) {
        if(staffDao.deleteStaffDao(staff_id)>0){
            return R.ok(null);
        }else {
            return R.fail("删除失败,请咨询CEO");
        }
    }
}
