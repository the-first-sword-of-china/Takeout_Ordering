package com.fendu.service.impl;

import com.alibaba.excel.EasyExcel;
import com.fendu.bo.TableBo;
import com.fendu.config.SystemContant;
import com.fendu.dao.TableDao;
import com.fendu.dto.TableExcelDto;
import com.fendu.entity.Table;
import com.fendu.listener.TableReadListener;
import com.fendu.service.intf.TableService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Service
public class TableServiceImpl implements TableService {
    @Autowired
    private TableDao tableDao;
    @Autowired
    private TableReadListener tableReadListener;

//新增
    @Override
    public R insertTableService(Table table) {
        if (tableDao.inertTableDao(table) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }
//查询
    @Override
    public PageVo<Table> selectTableService(int page, int limit) {

        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<Table> pageInfo=new PageInfo<>(tableDao.selectTableDao());

        return new PageVo<Table>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }
    //查询2
    @Override
    public PageVo<Table> selectTableService2(TableBo tableBo) {
        if(tableBo!=null){
            //1.组装查询条件
            Table table=new Table();
            //如果tableBo的编号有长度 意思是不为零
            if(StringUtils.hasLength(tableBo.getTable_numid())){
                //则赋值给table
                table.setTable_numid(tableBo.getTable_numid());
            }
            if(tableBo.getTable_state()>-1){
                table.setTable_state(tableBo.getTable_state());
            }
            if(tableBo.getTable_type()>-1){
                table.setTable_type(tableBo.getTable_type());
            }

            //2.开启分页插件
            PageHelper.startPage(tableBo.getPage(), tableBo.getLimit());
            //3.获取分页的查询结果
            PageInfo<Table> pageInfo=new PageInfo<>(tableDao.selectTableDao2(table));
            //4.返回结果
            return new PageVo<Table>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
        }
        return new PageVo<Table>(SystemContant.PAGE_R,"OK",0,null);
    }

    //修改
    @Override
    public R updateTableService(Table table) {
        if (tableDao.updateTableDao(table)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
//删除
    @Override
    public R deleteTableService(int table_id) {
        if(tableDao.deleteTableDao(table_id)>0){
            return R.ok(null);
        }else {
            return R.fail("删除失败,请咨询CEO");
        }
    }



    @Override
    public R batchAdd(MultipartFile file) {
        if(!file.isEmpty()){
            //excel解析
            try {
                EasyExcel.read(file.getInputStream(), TableExcelDto.class,tableReadListener).sheet().doRead();
                return R.ok("OK");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return R.fail();
    }

    @Override
    public void exportExcel(TableBo tableBo, HttpServletResponse response) {
        //1.设置响应消息头，目的：告诉请求方，这个是下载
        String fn = System.currentTimeMillis()+".xlsx";
        //设置响应头  告知浏览器，要以附件的形式保存内容   filename=浏览器显示的下载文件名
        response.setHeader("content-disposition","attachment;filename="+fn);
        //2.组装查询条件
        Table table=new Table();
        //如果tableBo的编号有长度 意思是不为零
        if(StringUtils.hasLength(tableBo.getTable_numid())){
            //则赋值给table
            table.setTable_numid(tableBo.getTable_numid());
        }
        if(tableBo.getTable_state()>-1){
            table.setTable_state(tableBo.getTable_state());
        }
        if(tableBo.getTable_type()>-1){
            table.setTable_type(tableBo.getTable_type());
        }
        //3.获取要下载的数据
        List<Table> list = tableDao.selectTableDao2(table);
        try {
            //生成excel，并将数据写出到响应流中
            EasyExcel.write(response.getOutputStream(),Table.class).sheet().doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
