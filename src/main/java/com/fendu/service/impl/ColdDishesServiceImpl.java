package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.ColdDishesDao;
import com.fendu.entity.ColdDishes;
import com.fendu.service.intf.ColdDishesService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service

public class ColdDishesServiceImpl implements ColdDishesService {
    @Autowired
    private ColdDishesDao  coldDishesDao;
    //新增
    @Override
    public R insterClodService(ColdDishes coldDishes) {
        if (coldDishesDao.insterClodDao(coldDishes) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    //查询
    public PageVo<ColdDishes> selectColdService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<ColdDishes> pageInfo = new PageInfo<>(coldDishesDao.selectColdDao());

        return new PageVo<ColdDishes>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }
//删除
    @Override
    public R deleteColdById(int id) {
        if(coldDishesDao.deleteColdById(id)>0){
            return R.ok("删除成功");
        }else {
            return R.fail("亲，有故障");
        }
    }

    //修改
    @Override
    public R updateCold(ColdDishes coldDishes) {
        if(coldDishesDao.updateColdById(coldDishes)>0){
            return R.ok("修改成功");
        }else {
            return R.fail("修改失败");
        }
    }
}
