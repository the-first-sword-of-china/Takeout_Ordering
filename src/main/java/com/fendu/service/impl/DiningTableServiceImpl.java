package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.DiningTableDao;
import com.fendu.entity.DiningTable;
import com.fendu.service.intf.DiningTableService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiningTableServiceImpl implements DiningTableService {

    @Autowired
    private DiningTableDao diningTabledao;

    //新增
    @Override
    public R insterDiningTable(DiningTable diningTable) {
        if(diningTabledao.insterDiningTable(diningTable)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //查询
    @Override
    public PageVo<DiningTable> selectDiningTableService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<DiningTable> pageInfo = new PageInfo<>(diningTabledao.selectDiningTableAll());

        return new PageVo<DiningTable>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    //删除
    @Override
    public R deleteDiningTableById(int id) {
        if(diningTabledao.deleteDiningTableById(id)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //修改
    @Override
    public R updateDiningTable(DiningTable diningTable) {
        if(diningTabledao.updateTableById(diningTable)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }
}
