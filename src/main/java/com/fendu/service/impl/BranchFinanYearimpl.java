package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.BranchFinanYearDao;
import com.fendu.entity.BranchFinanYear;
import com.fendu.entity.HotDishes;
import com.fendu.service.intf.BranchFinanYearService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BranchFinanYearimpl implements BranchFinanYearService {
    @Autowired
    private BranchFinanYearDao branchFinanYearDao;
    @Override
    public R insertBranchYearService(BranchFinanYear branchFinanYear) {
        branchFinanYear.setStores_name((branchFinanYear.getStores_name()));
        branchFinanYear.setFina_week_day(branchFinanYear.getFina_week_day());
        branchFinanYear.setFina_week_expend(branchFinanYear.getFina_week_expend());
        branchFinanYear.setFina_week_income(branchFinanYear.getFina_week_income());
        if (branchFinanYearDao.insertBranchYearDao(branchFinanYear)>0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public PageVo<BranchFinanYear> selectBranchYearService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<BranchFinanYear> pageInfo=new PageInfo<>(branchFinanYearDao.selectBranchYearDao());
        return new PageVo<BranchFinanYear>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }


    @Override
    public R updateBranchYearService(BranchFinanYear branchFinanYear) {
        System.out.println(branchFinanYear);
        if (branchFinanYearDao.updateBranchYearDao(branchFinanYear)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    @Override
    public R deleteBranchYearService(int finanyear_id) {
        if(branchFinanYearDao.deleteBranchYearDao(finanyear_id)>0){
            return R.ok(null);
        }else {
            return R.fail("删除失败,请咨询CEO");
        }
    }

}
