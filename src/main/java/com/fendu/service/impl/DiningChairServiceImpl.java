package com.fendu.service.impl;

import com.fendu.bo.DiningChairBo;
import com.fendu.bo.PageBo;
import com.fendu.config.SystemContant;
import com.fendu.dao.DiningChairDao;
import com.fendu.entity.DiningChair;
import com.fendu.service.intf.DiningChairService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class DiningChairServiceImpl implements DiningChairService {

    @Autowired
    private DiningChairDao diningChairDao;

    //新增
    @Override
    public R insterDiningChair(DiningChair diningChair) {
        if(diningChairDao.insterDiningChair(diningChair)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //查询
    @Override
    public PageVo<DiningChair> selectDiningChairService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<DiningChair> pageInfo = new PageInfo<>(diningChairDao.selectDiningChairAll());

        return new PageVo<DiningChair>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public PageVo<DiningChair> selectDiningChairByCd(DiningChairBo bo) {
        if(bo!=null){
            //组装查询条件
            DiningChair diningChair = new DiningChair();
//            if(StringUtils.hasLength(bo.getChair_numid())){
//                diningChair.setChair_numid(bo.getChair_numid());
//            }
//            if(StringUtils.hasLength(bo.getChair_type())){
//                diningChair.setChair_type(bo.getChair_type());
//            }
//            if(StringUtils.hasLength(bo.getChair_name())){
//                diningChair.setChair_name(bo.getChair_name());
//            }
            if(bo.getChair_numid()!=null){
                diningChair.setChair_numid(bo.getChair_numid());
            }
            if(bo.getChair_type()!=null){
                diningChair.setChair_type(bo.getChair_type());
            }
            if(bo.getChair_name()!=null){
                diningChair.setChair_name(bo.getChair_name());
            }
            //1.开启分页插件
            PageHelper.startPage(bo.getPage(),bo.getLimit());
            //2.获取分页的查询结果
            PageInfo<DiningChair> pageInfo = new PageInfo<>(diningChairDao.selectByCd(diningChair));

            return new PageVo<DiningChair>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
        }
        return new PageVo<DiningChair>(SystemContant.PAGE_R,"OK",0,null);
    }

    //删除
    @Override
    public R deleteDiningChairById(int id) {
        if(diningChairDao.deleteDiningChairById(id)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //修改
    @Override
    public R updateDiningChair(DiningChair diningChair) {
        if(diningChairDao.updateDiningChairById(diningChair)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }
}
