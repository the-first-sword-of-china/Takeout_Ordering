package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StockBeveRagesDao;
import com.fendu.entity.Meat;
import com.fendu.entity.StockBeverages;
import com.fendu.service.intf.StockBeveRagesService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 锤子
 * @data 2021/10/8 17:08
 * @projectname Takeout_Ordering02
 */
@Service
public class StockBeveRagesServiceImpl implements StockBeveRagesService {
    @Autowired
  private   StockBeveRagesDao  stockBeveRagesDao;
    @Override
    public PageVo<StockBeverages> selectBeveService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<StockBeverages> pageInfo=new PageInfo<>(stockBeveRagesDao.selectBevetDao());

        return new PageVo<StockBeverages>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());

    }

    @Override
    public R insertBeveService(StockBeverages stockBeverages) {
        if (stockBeveRagesDao.insterBeveDao(stockBeverages) > 0) {
            return R.ok(null);
        }else {
            return  R.fail();
        }
    }

    @Override
    public R delectBeveService(int beve_id) {
        if(stockBeveRagesDao.deleBeveDao(beve_id) > 0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    @Override
    public R updateBeveService(StockBeverages stockBeverages) {
        if (stockBeveRagesDao.updateBevetDao(stockBeverages)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
}
