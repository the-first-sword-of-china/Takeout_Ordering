package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.BeveragesDao;

import com.fendu.entity.Beverages;
import com.fendu.service.intf.BeveragesService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 锤子
 * @data 2021/10/5 23:55
 * @projectname LoginDemo
 */
@Service

public class BeveragesServiceImpl implements BeveragesService {
    //新增
    @Autowired
    private BeveragesDao beveragesDao;
    @Override
    public R insterBeverService(Beverages beverages) {
        if (beveragesDao.insterBeverDao(beverages) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }
    //查询展示
    @Override
    public PageVo<Beverages> selectBeverService(int page,int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<Beverages> pageInfo=new PageInfo<>(beveragesDao.selectBeverDao());


        return new PageVo<Beverages>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }
}
