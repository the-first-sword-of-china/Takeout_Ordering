package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.MeatDao;
import com.fendu.dao.RatePersonDao;
import com.fendu.entity.Meat;
import com.fendu.entity.RatePerson;
import com.fendu.service.intf.MeatService;
import com.fendu.service.intf.RatePersonService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 锤子
 * @data 2021/10/5 23:55
 * @projectname LoginDemo
 */
@Service

public class RatePersonServiceImpl implements RatePersonService {
    @Autowired
    private RatePersonDao  ratePersonDao;

    @Override
    public PageVo<RatePerson> selectPersonService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<RatePerson> pageInfo=new PageInfo<>(ratePersonDao.selectPersonDao());

        return new PageVo<RatePerson>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public R insertPersonService(RatePerson ratePerson) {
        if (ratePersonDao.insterPersonDao(ratePerson) > 0) {
            return R.ok(null);
        }else {
            return  R.fail();
        }
    }

    @Override
    public R delectPersonService(int rate_id) {
        if(ratePersonDao.delePersonDao(rate_id) > 0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    @Override
    public R updatePersonService(RatePerson ratePerson) {
        if (ratePersonDao.updatePersonDao(ratePerson)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

}
