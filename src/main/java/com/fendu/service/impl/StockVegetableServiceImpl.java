package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StockVegetableDao;
import com.fendu.entity.StockSeafood;
import com.fendu.entity.StockVegetable;
import com.fendu.service.intf.StockVegetableService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockVegetableServiceImpl implements StockVegetableService {
    @Autowired
    private StockVegetableDao stockVegetableDao;

    //增加
    @Override
    public R insterSVegetable(StockVegetable stockVegetable) {
        if(stockVegetableDao.insterVegetable(stockVegetable)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //查询
    @Override
    public PageVo<StockVegetable> selectStockVegAll(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<StockVegetable> pageInfo = new PageInfo<>(stockVegetableDao.selectVegetableAll());

        return new PageVo<StockVegetable>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    //删除
    @Override
    public R deleteVegetableById(int id) {
        if(stockVegetableDao.deleteVegetableById(id)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //修改
    @Override
    public R updateVegetable(StockVegetable stockVegetable) {
        if(stockVegetableDao.updateStockVegetable(stockVegetable)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }
}
