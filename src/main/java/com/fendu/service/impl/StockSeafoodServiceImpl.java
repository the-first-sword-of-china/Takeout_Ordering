package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StockSeafoodDao;
import com.fendu.entity.StockSeafood;
import com.fendu.service.intf.StockSeafoodService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockSeafoodServiceImpl implements StockSeafoodService {

    @Autowired
    private StockSeafoodDao stockSeafoodDao;

    //增加
    @Override
    public R insterSeafood(StockSeafood stockSeafood) {
        if(stockSeafoodDao.insterSeafood(stockSeafood)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //查询
    @Override
    public PageVo<StockSeafood> selectStockSeafoodAll(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<StockSeafood> pageInfo = new PageInfo<>(stockSeafoodDao.selectSeafoodAll());

        return new PageVo<StockSeafood>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    //删除
    @Override
    public R deleteSeafoodById(int id) {
        if(stockSeafoodDao.deleteSeafoodById(id)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //修改
    @Override
    public R updateSeafood(StockSeafood stockSeafood) {
        if(stockSeafoodDao.updateSeafoodById(stockSeafood)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }
}
