package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.BranchStorePersonDao;
import com.fendu.entity.BranchFinanYear;
import com.fendu.entity.BranchStorePerson;
import com.fendu.service.intf.BranchFinanYearService;
import com.fendu.service.intf.BranchStorePersonService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BranchStorePresonServiceImpl implements BranchStorePersonService {
    @Autowired
    private BranchStorePersonDao branchStorePersonDao;


    @Override
    public R insertService(BranchStorePerson branchStorePerson) {
        if (branchStorePersonDao.insertDao(branchStorePerson)>0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public PageVo<BranchStorePerson> selectService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<BranchStorePerson> pageInfo=new PageInfo<>(branchStorePersonDao.selectDao());
        return new PageVo<BranchStorePerson>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public R updateService(BranchStorePerson branchStorePerson) {
        if (branchStorePersonDao.updateDao(branchStorePerson)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }

    @Override
    public R deleteService(int storePerson_id) {
        if (branchStorePersonDao.deleteDao(storePerson_id)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
}
