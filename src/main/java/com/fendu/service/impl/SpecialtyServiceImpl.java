package com.fendu.service.impl;
import com.fendu.config.SystemContant;
import com.fendu.dao.SpecialtyDao;
import com.fendu.entity.Specialty;
import com.fendu.service.intf.SpecialtyService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//声明Service层
@Service("specialtyService")
public class SpecialtyServiceImpl implements SpecialtyService {
    //自动注入Dao层
    @Autowired
    private SpecialtyDao specialtyDao;
    //新增
    @Override
    public R add(Specialty specialty) {
        if (specialtyDao.addDao(specialty) > 0) {
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
    //查询
    @Override
    public PageVo<Specialty> selectAll(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<Specialty> pageInfo=new PageInfo<>(specialtyDao.selectAllDao());
        return new PageVo<Specialty>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }
    //修改
    @Override
    public R edit(Specialty specialty) {

        if (specialtyDao.update(specialty)>0){
            return R.ok("OK");
        }else {
            return R.fail();
        }
    }
    //删除
    @Override
    public R dele(int id) {
        if(specialtyDao.delete(id)>0){
            return R.ok("删除成功");
        }else {
            return R.fail("亲，有故障");
        }
    }

    }



