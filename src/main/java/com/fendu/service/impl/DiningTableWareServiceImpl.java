package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.DiningTableWareDao;
import com.fendu.entity.DiningTableWare;
import com.fendu.service.intf.DiningTableWareService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiningTableWareServiceImpl implements DiningTableWareService {

    @Autowired
    private DiningTableWareDao diningTableWareDao;

    //新增
    @Override
    public R insterDiningWare(DiningTableWare diningTableWare) {
        if(diningTableWareDao.insterDiningWare(diningTableWare)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //查询
    @Override
    public PageVo<DiningTableWare> selectDiningWareService(int page, int limit) {
        //1.开启分页插件
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<DiningTableWare> pageInfo = new PageInfo<>(diningTableWareDao.selectWareAll());

        return new PageVo<DiningTableWare>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }

    //删除
    @Override
    public R deleteTableWareById(int id) {
        if(diningTableWareDao.deleteTableWareById(id)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

    //修改
    @Override
    public R updateDiningWare(DiningTableWare diningTableWare) {
        if(diningTableWareDao.updateWareById(diningTableWare)>0){
            return R.ok("成功");
        }else {
            return R.fail();
        }
    }

}
