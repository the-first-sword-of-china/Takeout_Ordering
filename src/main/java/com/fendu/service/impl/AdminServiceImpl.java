package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.AdminDao;
import com.fendu.entity.Admin;
import com.fendu.service.intf.AdminService;
import com.fendu.util.EncryptUtil;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Objects;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminDao dao;

    //登录
    public R login(String adminName, String adminPassword, HttpSession session) {
        //采用AES加密方式
        //1.查询数据库,返回一个Admin对象
        Admin admin=dao.selectByName(adminName);
        //2.验证用户是否存在
        if(admin!=null){
            //3.校验状态
            if(admin.getAdminState()== SystemContant.ADMIN_STATE_OK){
                //4.校验密码，密文
                if(Objects.equals(admin.getAdminPassword(), EncryptUtil.aesenc(SystemContant.PASS_KEY,adminPassword))){
                    session.setAttribute("curruser",admin);
                    //5.登陆成功
                    return R.ok(admin);
                }else {
                    return R.fail("密码有误!");
                }
            }else {
                return R.fail("当前账号不可用!");
            }

        }else {
            return R.fail("账号不存在!");
        }
    }

    //账户修改
    @Override
    public R update(Admin admin) {
        admin.setAdminPassword(EncryptUtil.aesenc(SystemContant.PASS_KEY,admin.getAdminPassword()));
        if(dao.editAdminDao(admin)>0){
            return R.ok("ok");
        }else {
            return R.fail();
        }
    }

    //账户修改密码
    @Override
    public R changePassword(String oldPassword, String newPassword, HttpSession session) {
        //1.获取当前的登陆账号
        Admin admin = (Admin)session.getAttribute("curruser");
        if(admin!=null){
            //2.验证旧密码是否一致
            if(Objects.equals(admin.getAdminPassword(),EncryptUtil.aesenc(SystemContant.PASS_KEY,oldPassword))){
                //3.更新密码
                if(dao.updatePasswordDao(admin.getAdminId(),EncryptUtil.aesenc(SystemContant.PASS_KEY,newPassword))>0){
                    //4.清空登陆信息，重新登陆
                    session.removeAttribute("curruser");
                    return R.ok("OK");
                }
            }
        }
        return R.fail();
    }
}
