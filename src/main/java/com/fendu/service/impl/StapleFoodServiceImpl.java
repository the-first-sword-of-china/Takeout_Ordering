package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StapleFoodDao;
import com.fendu.entity.StapleFood;
import com.fendu.service.intf.StapleFoodService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class  StapleFoodServiceImpl implements StapleFoodService {
    @Autowired
    private StapleFoodDao stapleFoodDao;
    @Override
    public PageVo<StapleFood> selectStapleFoodService(int page, int limit) {
        //开启分页
        PageHelper.startPage(page,limit);
        PageInfo<StapleFood> pageInfo = new PageInfo<>(stapleFoodDao.selectStapleFoodDao());

        return new PageVo<StapleFood>(SystemContant.PAGE_R,"OK",pageInfo.getTotal(),pageInfo.getList());
    }
    //增加
    @Override
    public R insertStapleFoodService(StapleFood stapleFood) {
        if (stapleFoodDao.insterStapleFoodDao(stapleFood) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }
    //修改
    @Override
    public R updateStapleFoodService(StapleFood stapleFood) {
        if (stapleFoodDao.updateStapleFoodDao(stapleFood)>0){
            return R.ok(null);
        }else {
            return R.fail();
        }
    }
    //删除
    @Override
    public R deleteStapleFoodService(int staple_id) {
        if(stapleFoodDao.deleteStapleFoodDao(staple_id)>0){
            return R.ok(null);
        }else {
            return R.fail("删除失败,请咨询CEO");
        }
    }
    }

