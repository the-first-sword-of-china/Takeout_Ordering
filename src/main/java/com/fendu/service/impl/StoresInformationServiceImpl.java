package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.StoresInformationDao;
import com.fendu.entity.FinaWeek;
import com.fendu.entity.StoresInformation;
import com.fendu.service.intf.StoresInformationService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoresInformationServiceImpl implements StoresInformationService {
    @Autowired
    private StoresInformationDao storesInformationDao;
    @Override
    public PageVo<StoresInformation> selectStoresInformationService(int page, int limit) {
        PageHelper.startPage(page,limit);
        PageInfo<StoresInformation> pageInfo = new PageInfo<>(storesInformationDao.selectStoresInformationDao());
        return new PageVo<StoresInformation>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public R insertStoresInformationService(StoresInformation storesInformation) {
        if (storesInformationDao.insertStoresInformationDao(storesInformation) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public R updateStoresInformationService(StoresInformation storesInformation) {
        if (storesInformationDao.updateStoresInformationDao(storesInformation) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }

    @Override
    public R deleteStoresInformationService(int stores_id) {
        if (storesInformationDao.deleteStoresInformationDao(stores_id) > 0) {
            return R.ok(null);
        } else {
            return R.fail();
        }
    }
}
