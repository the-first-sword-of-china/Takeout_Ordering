package com.fendu.service.impl;

import com.fendu.config.SystemContant;
import com.fendu.dao.BranchFinanWeekDao;
import com.fendu.entity.BranchFinanWeek;
import com.fendu.service.intf.BranchFinanWeekService;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//声明Service层
@Service("BranchFinanWeekService")
public class BranchFinanWeekServiceImpl implements BranchFinanWeekService {
    @Autowired
    private BranchFinanWeekDao branchFinanWeekDao;
    //增
    @Override
    public R addService(BranchFinanWeek branchFinanWeek) {
        if (branchFinanWeekDao.addDao(branchFinanWeek)>0){
            return R.ok(null);
        }
        return R.fail();
    }
    //删
    @Override
    public R delService(int id) {
        if (branchFinanWeekDao.delDao(id)>0){
            return R.ok(null);
        }
        return R.fail();
    }
    //改
    @Override
    public R updService(BranchFinanWeek branchFinanWeek) {
        if (branchFinanWeekDao.updDao(branchFinanWeek)>0){
            return R.ok(null);
        }
        return R.fail();
    }
    //查
    @Override
    public PageVo<BranchFinanWeek> selService(int page, int limit) {
        //开启分页查询
        PageHelper.startPage(page, limit);
        //2.获取分页的查询结果
        PageInfo<BranchFinanWeek>pageInfo=new PageInfo<>(branchFinanWeekDao.selDao());
        return new PageVo<BranchFinanWeek>(SystemContant.PAGE_R,"ok",pageInfo.getTotal(),pageInfo.getList());
    }
}
