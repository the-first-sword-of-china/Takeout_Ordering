package com.fendu.service.intf;

import com.fendu.entity.DiningTableWare;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface DiningTableWareService {
    //新增
    R insterDiningWare(DiningTableWare diningTableWare);

    //查询
    PageVo<DiningTableWare> selectDiningWareService(int page, int limit);

    //删除
    R deleteTableWareById(int id);

    //修改
    R updateDiningWare(DiningTableWare diningTableWare);
}
