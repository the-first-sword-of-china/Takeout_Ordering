package com.fendu.service.intf;



import com.fendu.entity.Drink;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface DrinkService {
    //查询
    Drink select(int beve_id);
    //增加
    R save(Drink drink);
    PageVo<Drink> queryPage(int page,int limit);

    R delete(int beve_numid);
    R update(Drink drink);
}
