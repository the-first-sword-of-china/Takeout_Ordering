package com.fendu.service.intf;

import com.fendu.entity.StockVegetable;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface StockVegetableService {
    //新增
    R insterSVegetable(StockVegetable stockVegetable);

    //查询
    PageVo<StockVegetable> selectStockVegAll(int page, int limit);

    //删除
    R deleteVegetableById(int id);

    //修改
    R updateVegetable(StockVegetable stockVegetable);
}
