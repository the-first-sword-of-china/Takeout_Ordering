package com.fendu.service.intf;

import com.fendu.entity.BranchFinanYear;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface BranchFinanYearService {
    R insertBranchYearService(BranchFinanYear branchFinanYear);

    PageVo<BranchFinanYear> selectBranchYearService(int page,int limit);

    R updateBranchYearService(BranchFinanYear branchFinanYear);


    R deleteBranchYearService(int finanyear_id);
}
