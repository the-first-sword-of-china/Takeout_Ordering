package com.fendu.service.intf;

import com.fendu.entity.BranchFinanMonth;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface BranchFinanMonthService {
    //增
    R addService(BranchFinanMonth branchFinanMonth);
    //删
    R delService(int id);
    //改
    R updService(BranchFinanMonth branchFinanMonth);
    //查
    PageVo<BranchFinanMonth> selService(int page, int limit);

}
