package com.fendu.service.intf;

import com.fendu.entity.FinaWeek;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface FinaWeekService {
    //    查询
    PageVo<FinaWeek> selectFinaWeekService(int page, int limit);
    //    新增
    R insertFinaWeekService(FinaWeek finaWeek);

    //    修改
    R updateFinaWeekService(FinaWeek finaWeek);
    //    删除
    R deleteFinaWeekService(int fina_week_id);
}
