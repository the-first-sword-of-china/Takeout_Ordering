package com.fendu.service.intf;

import com.fendu.entity.FinaWeek;
import com.fendu.entity.StoresInformation;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface StoresInformationService {
    //    查询
    PageVo<StoresInformation> selectStoresInformationService(int page, int limit);
    //    新增
    R insertStoresInformationService(StoresInformation storesInformation);

    //    修改
    R updateStoresInformationService(StoresInformation storesInformation);
    //    删除
    R deleteStoresInformationService(int stores_id);
}
