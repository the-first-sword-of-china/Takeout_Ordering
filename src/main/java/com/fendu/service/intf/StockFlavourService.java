package com.fendu.service.intf;


import com.fendu.entity.StockBeverages;
import com.fendu.entity.StockFlavour;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;


/**
 * @author 锤子
 * @data 2021/10/5 23:53
 * @projectname LoginDemo
 */

public interface StockFlavourService {

    PageVo<StockFlavour> selectFlavoService(int page, int limit);

    R insertFlavoService(StockFlavour stockFlavour);

    R delectFlavoService(int flavo_id);

    R updateFlavoService(StockFlavour stockFlavour);
}
