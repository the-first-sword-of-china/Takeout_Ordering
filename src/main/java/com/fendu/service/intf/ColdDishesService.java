package com.fendu.service.intf;

import com.fendu.entity.ColdDishes;
import com.fendu.entity.HotDishes;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface ColdDishesService {
    //新增
    R insterClodService(ColdDishes coldDishes);

    //查询
    PageVo<ColdDishes> selectColdService(int page, int limit);

    //删除
    R deleteColdById(int id);

    //修改
    R updateCold(ColdDishes coldDishes);
}
