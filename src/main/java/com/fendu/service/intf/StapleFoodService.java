package com.fendu.service.intf;

import com.fendu.entity.StapleFood;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public interface StapleFoodService {
    //查询并展示
    PageVo<StapleFood> selectStapleFoodService(int page, int limit);
    //新增
    R insertStapleFoodService(StapleFood stapleFood);
    //修改
    R updateStapleFoodService(StapleFood stapleFood);
    //删除
    R deleteStapleFoodService(int staple_id);
}
