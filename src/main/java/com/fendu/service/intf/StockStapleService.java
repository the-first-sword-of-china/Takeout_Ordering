package com.fendu.service.intf;

import com.fendu.entity.StockStapleFood;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

/**
 * @author 锤子
 * @data 2021/10/5 23:53
 * @projectname LoginDemo
 */

public interface StockStapleService {

    //新增
    R insertStockStapleService(StockStapleFood stockStapleFood);
    //查询并展示
    PageVo<StockStapleFood> selectStockStapleService(int page, int limit);
    //修改
    R updateStockStapleService(StockStapleFood stockStapleFood);
    //删除
    R deleteStockStapleService(int staple_id);
}
