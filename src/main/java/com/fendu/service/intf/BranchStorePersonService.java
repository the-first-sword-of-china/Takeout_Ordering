package com.fendu.service.intf;


import com.fendu.entity.BranchStorePerson;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface BranchStorePersonService {
    R insertService(BranchStorePerson branchStorePerson);

    PageVo<BranchStorePerson> selectService(int page,int limit);

    R updateService(BranchStorePerson branchStorePerson);


    R deleteService(int storePerson_id);
}
