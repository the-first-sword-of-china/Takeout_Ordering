package com.fendu.service.intf;

import com.fendu.entity.Customer;
import com.fendu.entity.Staff;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import com.github.pagehelper.Page;

public interface StaffService {
    //    查询
    PageVo<Staff> selectStaffService(int page, int limit);
    //    新增
    R insertStaffService(Staff staff);

    //    修改
    R updateStaffService(Staff staff);
    //    删除
    R deleteStaffService(int staff_id);
}
