package com.fendu.service.intf;
import com.fendu.entity.Specialty;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface SpecialtyService {
    //添加
    R add(Specialty specialty);
    //查询
    PageVo<Specialty> selectAll(int page, int limit);
    //修改
    R edit(Specialty specialty);
    //删除
    R dele(int id);

}
