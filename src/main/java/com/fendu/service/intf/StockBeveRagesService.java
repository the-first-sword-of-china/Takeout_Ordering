package com.fendu.service.intf;


import com.fendu.entity.Meat;
import com.fendu.entity.StockBeverages;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import lombok.Data;
import org.springframework.stereotype.Service;


/**
 * @author 锤子
 * @data 2021/10/5 23:53
 * @projectname LoginDemo
 */

public interface StockBeveRagesService {
//    //查询展示
//    PageVo<Meat> selectMeatService(int page, int limit);
//    //新增
//    R insertMeatService(Meat meat);
//    //删除
//    R delectMeatService(int meat_id);
//
//    R updateMeatService(Meat meat);


    PageVo<StockBeverages> selectBeveService(int page, int limit);

    R insertBeveService(StockBeverages stockBeverages);

    R delectBeveService(int beve_id);

    R updateBeveService(StockBeverages stockBeverages);
}
