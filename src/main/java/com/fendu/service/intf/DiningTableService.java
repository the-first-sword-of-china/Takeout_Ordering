package com.fendu.service.intf;

import com.fendu.entity.DiningTable;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface DiningTableService {
    //新增
    R insterDiningTable(DiningTable diningTable);

    //查询
    PageVo<DiningTable> selectDiningTableService(int page, int limit);

    //删除
    R deleteDiningTableById(int id);

    //修改
    R updateDiningTable(DiningTable diningTable);

}
