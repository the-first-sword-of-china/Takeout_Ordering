package com.fendu.service.intf;


import com.fendu.entity.Meat;
import com.fendu.entity.RatePerson;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;


/**
 * @author 锤子
 * @data 2021/10/5 23:53
 * @projectname LoginDemo
 */

public interface RatePersonService {
//    //查询展示
//    PageVo<Meat> selectMeatService(int page, int limit);
//    //新增
//    R insertMeatService(Meat meat);
//    //删除
//    R delectMeatService(int meat_id);
//
//    R updateMeatService(Meat meat);

    PageVo<RatePerson> selectPersonService(int page, int limit);

    R insertPersonService(RatePerson ratePerson);

    R delectPersonService(int rate_id);

    R updatePersonService(RatePerson ratePerson);



}
