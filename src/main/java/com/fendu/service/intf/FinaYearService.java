package com.fendu.service.intf;

import com.fendu.entity.FinaMonth;
import com.fendu.entity.FinaYear;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface FinaYearService {
    //    查询
    PageVo<FinaYear> selectFinaYearService(int page, int limit);
    //    新增
    R insertFinaYearService(FinaYear finaYear);

    //    修改
    R updateFinaYearService(FinaYear finaYear);
    //    删除
    R deleteFinaYearService(int fina_year_id);
}
