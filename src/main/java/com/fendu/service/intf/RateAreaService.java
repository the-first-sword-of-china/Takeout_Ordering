package com.fendu.service.intf;


import com.fendu.entity.RateArea;
import com.fendu.entity.RatePerson;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;


/**
 * @author 锤子
 * @data 2021/10/5 23:53
 * @projectname LoginDemo
 */

public interface RateAreaService {

    PageVo<RateArea> selectAreaService(int page, int limit);

    R insertAreaService(RateArea area);

    R delectAreaService(int area_id);

    R updateAreaService(RateArea rateArea);
}
