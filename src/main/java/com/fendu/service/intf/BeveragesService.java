package com.fendu.service.intf;

import com.fendu.entity.Beverages;
import com.fendu.entity.HotDishes;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

/**
 * @author 锤子
 * @data 2021/10/5 23:53
 * @projectname LoginDemo
 */

public interface BeveragesService {
    R insterBeverService(Beverages beverages);

    PageVo<Beverages> selectBeverService(int page, int limit);


}
