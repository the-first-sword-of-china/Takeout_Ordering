package com.fendu.service.intf;

import com.fendu.entity.StoreRate;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface StoreRateService {
    //增
    R addService(StoreRate storeRate);
    //删
    R delService(int id);
    //改
    R updService(StoreRate storeRate);
    //查
    PageVo<StoreRate> selService(int page, int limit);
}
