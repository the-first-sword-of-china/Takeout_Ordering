package com.fendu.service.intf;

import com.fendu.entity.HavePaid;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface HavePaidService {
    //增
    R addService(HavePaid havePaid);
    //删
    R delService(int id);
    //改
    R updService(HavePaid havePaid);
    //查
    PageVo<HavePaid> selService(int page, int limit);

}
