package com.fendu.service.intf;

import com.fendu.bo.TableBo;
import com.fendu.entity.Table;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface TableService {
//    新增
    R insertTableService(Table table);
//    查询展示
    PageVo<Table> selectTableService(int page, int limit);
//    查询2.0
    PageVo<Table> selectTableService2(TableBo tableBo);
//    修改
    R updateTableService(Table table);
//     删除
    R deleteTableService(int table_id);
    //文件上传
    R batchAdd(MultipartFile file);
    //导出excel文件
    void exportExcel(TableBo tableBo, HttpServletResponse response);
}
