package com.fendu.service.intf;


import com.fendu.entity.NotPaid;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;


public interface NotPaidService {
    //增
    R addService(NotPaid NotPaid);
    //删
    R delService(int id);
    //改
    R updService(NotPaid notPaid);
    //查
    PageVo<NotPaid> selService(int page, int limit);
}
