package com.fendu.service.intf;

import com.fendu.entity.HotDishes;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

/**
 * @author 锤子
 * @data 2021/10/5 23:53
 * @projectname LoginDemo
 */

public interface HotDishesService {
    //新增
    R insertHotService(HotDishes hotDishes);
    //查询展示
    PageVo<HotDishes> selectHotService(int page, int limit);
    //修改
    R updateHotService(HotDishes hotDishes);

    R deleteHotService(int hot_id);

}
