package com.fendu.service.intf;

import com.fendu.entity.Admin;
import com.fendu.vo.R;

import javax.servlet.http.HttpSession;

public interface AdminService {

    R login(String adminName, String adminPassword, HttpSession session);
    //修改管理员资料 未实现
    R update(Admin admin);
    //修改密码 未实现
    R changePassword(String oldPassword,String newPassword,HttpSession session);
}
