package com.fendu.service.intf;

import com.fendu.entity.BranchFinanWeek;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface BranchFinanWeekService {
    //增
    R addService(BranchFinanWeek branchFinanWeek);
    //删
    R delService(int id);
    //改
    R updService(BranchFinanWeek branchFinanWeek);
    //查
    PageVo<BranchFinanWeek> selService(int page, int limit);

}
