package com.fendu.service.intf;

import com.fendu.bo.DiningChairBo;
import com.fendu.entity.DiningChair;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface DiningChairService {
    //新增
    R insterDiningChair(DiningChair diningChair);

    //查询
    PageVo<DiningChair> selectDiningChairService(int page, int limit);

    //条件查询
    PageVo<DiningChair> selectDiningChairByCd(DiningChairBo bo);
    //删除
    R deleteDiningChairById(int id);

    //修改
    R updateDiningChair(DiningChair diningChair);
}
