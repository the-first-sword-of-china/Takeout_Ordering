package com.fendu.service.intf;

import com.fendu.bo.FinaMonthBo;
import com.fendu.entity.FinaMonth;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface FinaMonthService {
    //    查询
    PageVo<FinaMonth> selectFinaMonthService(int page, int limit);
    PageVo<FinaMonth> selectFinaMonthWhereService(FinaMonthBo finaMonthBo);
    //    新增
    R insertFinaMonthService(FinaMonth finaMonth);

    //    修改
    R updateFinaMonthService(FinaMonth finaMonth);
    //    删除
    R deleteFinaMonthService(int fina_mon_id);
    R batchAdd(MultipartFile multipartFile);
    void exportExcel(FinaMonthBo finaMonthBo, HttpServletResponse httpServletResponse);
}
