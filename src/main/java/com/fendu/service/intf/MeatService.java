package com.fendu.service.intf;


import com.fendu.entity.Meat;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;


/**
 * @author 锤子
 * @data 2021/10/5 23:53
 * @projectname LoginDemo
 */

public interface MeatService {
    //查询展示
    PageVo<Meat> selectMeatService(int page, int limit);
    //新增
    R insertMeatService(Meat meat);
    //删除
    R delectMeatService(int meat_id);

    R updateMeatService(Meat meat);


//    //新增
//    R insertHotService(HotDishes hotDishes);
//    //查询展示
//    PageVo<HotDishes> selectHotService(int page, int limit);
//    //修改
//    R updateHotService(HotDishes hotDishes);
//
//    R deleteHotService(int hot_id);

}
