package com.fendu.service.intf;

import com.fendu.entity.StockSeafood;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface StockSeafoodService {
    //新增
    R insterSeafood(StockSeafood stockSeafood);

    //查询
    PageVo<StockSeafood> selectStockSeafoodAll(int page, int limit);

    //删除
    R deleteSeafoodById(int id);

    //修改
    R updateSeafood(StockSeafood stockSeafood);
}
