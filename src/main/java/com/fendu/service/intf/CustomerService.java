package com.fendu.service.intf;

import com.fendu.entity.Customer;
import com.fendu.entity.Table;
import com.fendu.vo.PageVo;
import com.fendu.vo.R;

public interface CustomerService {
    //    新增
    R insertCustomerService(Customer customer);
    //    查询展示
    PageVo<Customer> selectCustomerService(int page, int limit);
    //    修改
    R updateCustomerService(Customer customer);
    //     删除
    R deleteCustomerService(int cust_id);
}
