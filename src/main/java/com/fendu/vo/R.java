package com.fendu.vo;

import com.fendu.config.SystemContant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data//自动get set方法
@AllArgsConstructor//全参构造
@NoArgsConstructor//无参构造
public class R {
    private int code;//状态码
    private String msg;//状态码说明
    private Object data;//数据

//    //成功的返回
//    public static R ok(int code, String msg, Object data) {
//        return new R(SystemContant.R_OK, msg, data);
//    }
//
//    //失败的返回
//    public static R fail(int cod, String msg, Object data) {
//        return new R(SystemContant.R_FAIL, msg, data);
//    }

    //成功
    public static R ok(String msg, Object data){
        return new R(SystemContant.R_OK,msg,data);
    }

    public static R ok( Object data){
        return new R(SystemContant.R_OK,"OK",data);
    }


    //失败
    public static R fail(String msg){
        return new R(SystemContant.R_FAIL,msg,null);
    }

    public static R fail(){
        return new R(SystemContant.R_FAIL,"FAIL",null);
    }
}
