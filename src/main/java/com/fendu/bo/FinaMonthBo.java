package com.fendu.bo;

import lombok.Data;

@Data
public class FinaMonthBo extends PageBo{
    private double fina_mon_extend;
    private double fina_mon_income;
    private double fina_mon_profit;
}
