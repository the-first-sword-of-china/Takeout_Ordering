package com.fendu.bo;

import lombok.Data;

@Data
public class PageBo {
    private int page;
    private int limit;
}
