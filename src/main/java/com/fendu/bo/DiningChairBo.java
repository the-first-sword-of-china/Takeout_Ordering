package com.fendu.bo;

import lombok.Data;

@Data
public class DiningChairBo extends PageBo{
    private String chair_numid;//订单编号
    private String chair_type;//类型
    private String chair_name;//购买人
}
