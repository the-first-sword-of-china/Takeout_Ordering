package com.fendu.bo;

import lombok.Data;

@Data
public class TableBo extends PageBo{
    private String table_numid;
    private int table_type;
    private int table_state;
}
